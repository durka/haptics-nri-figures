function amazon_summarize_conf(sp, c, t, nx, ny, fs)
    if nargin < 6
        fs = 12;
        if nargin < 5
            ny = nx;
        end
    end

    subplot(sp);
    cla;
    imagesc(abs(c), [0 1]);
    colormap(flipud(gray));
    for i=1:size(c,1)
        for j=1:size(c,2)
            if abs(c(i,j)) < 0.5
                color = 'black';
            else
                color = 'white';
            end
            text(j,i, sprintf('%.2f', c(i,j)), 'Color',color, 'FontWeight','bold', 'HorizontalAlignment','center', 'FontSize',fs);
        end
    end
    
    axis equal tight;
    set(gca, 'FontSize', fs);
    set(gca, 'XTick', 1:length(nx));
    set(gca, 'XTickLabel', nx);
    set(gca, 'YTick', 1:length(ny));
    set(gca, 'YTickLabel', ny);
    
    % copy axis labels
    ax = gca;
    axes('Position',ax.Position, 'Color','none', 'FontSize',fs, ...
         'XAxisLocation','top', 'YAxisLocation','right', ...
         'XLim',ax.XLim, 'YLim',ax.YLim, ...
         'DataAspectRatio',ax.DataAspectRatio, ...
         'XTick',ax.XTick, 'YTick',ax.YTick, ...
         'XTickLabel',ax.XTickLabel, 'YTickLabel',flipud(ax.YTickLabel));

    set(gcf, 'CurrentAxes',ax);
    title({t, ''});
end


