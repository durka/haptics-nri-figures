function [ep, taps, info] = icra17_process_taps(ep)
    [pks, taps, w, p] = findpeaks(ep.biws(:,4) ...
                          , 'MinPeakProminence', 10 ...
                          , 'MaxPeakWidth', 1000 ...
                          , 'MinPeakDistance', 500 ...
                          , 'NPeaks',10, 'SortStr','descend');
    [taps,sortidx] = sort(taps);
    pks = pks(sortidx);
    w = w(sortidx);
    p = p(sortidx);
    good_taps = false(size(pks));
    maxdist = 8000;
    for i=1:length(taps)
        if     (i > 1 && taps(i)-taps(i-1) < maxdist)               ...
            || (i == 1 && taps(i) < maxdist)                        ...
            || (i < length(taps) && taps(i+1)-taps(i) < maxdist)    ...
            || (i == length(taps) && size(ep.biws,1)-taps(i) < maxdist)
            good_taps(i) = true;
        end
    end
    info = {pks taps w p good_taps};
    pks = pks(good_taps);
    taps = taps(good_taps);
    w = w(good_taps);
    p = p(good_taps);
    ep.tmask = false(size(ep.bvei,1),1);
    for i=1:length(taps)
        a = max(1, taps(i)-750);
        b = min(taps(i)+750, length(ep.tmask));
        ep.tmask(a:b) = 1;
    end
    ep.smask = ~ep.tmask;
end
