function t = fudge_clock_skips(t)
    skip_locs = find(diff(t) < 0);
    for s=1:length(skip_locs)
        loc = skip_locs(s);
        skip = t(loc+1) - t(loc);
        before = t(loc) - t(loc-1);
        t(loc+1:end) = t(loc+1:end) - skip + before;
    end
end

