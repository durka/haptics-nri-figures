function print_subplots(filename, fs)

    if nargin < 2
        fs = 13;
    end

    axs = findobj(gcf, 'type','axes');
    xps = [];
    yps = [];
    for i=1:length(axs)
        xps(i) = axs(i).Position(1);
        yps(i) = axs(i).Position(2);
    end
    w = numel(unique(xps));
    h = numel(unique(yps));
    n = length(axs);

    for i=1:n
        disp(i);
        
        % slurp some info about the plot we're copying
        cmap = colormap;
        has_legend = ~isempty(get(subplot(h,w,i), 'Legend'));
        outerpos = get(subplot(h,w,i), 'OuterPosition');
        old_w = outerpos(3);
        old_h = outerpos(4);
        
        % copy it
        copyobj(subplot(h,w,i), figure);
        
        % expand to fit old aspect ratio
        figpos = get(gcf, 'Position');
        new_h = figpos(4);
        new_w = old_w/old_h*new_h;
        set(gcf, 'Position', [figpos(1:2) new_w new_h]);
        
        % wide plots fill the page better as landscape
        if new_w > new_h
            set(gcf, 'PaperOrientation', 'landscape');
        end
        
        % restore some details
        colormap(cmap);
        if has_legend
            legend(gca, 'show');
        end
        set(gca, 'OuterPosition',[0 0 1 1], ...
                 'FontSize', fs);
        set(findall(gca, 'Type','text'), 'FontSize',fs);
        
        [~,~,ext] = fileparts(filename);
        print('-bestfit', ['-d' ext(2:end)], sprintf(filename, i));
        
        close;
    end

end
