function [table, removed] = filter_table(table, varargin)

    rows = true(height(table), 1);
    
    for i=1:length(varargin)
        rows(~feval(varargin{i}, table)) = 0;
    end
    
    removed = table(rows,:);
    table(rows,:) = [];

end
