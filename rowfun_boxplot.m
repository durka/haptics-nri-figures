function meancorr = rowfun_boxplot(table, y, x, ti, xl, yl)

    ty = ['turk_' y];
    ey = ['exp_' y];

    ord = rowfun(@mean, table, 'GroupingVariable',x, 'InputVariables',ty, 'OutputVariableName','OUTPUT');
    ord = sortrows(ord, 'OUTPUT');
    boxplot(table.(ty), table.(x), ...
            'plotstyle','compact', 'grouporder',ord.(x), 'labels',table.(x));

    % plot means and experimenter rating
    tmeans = ord.OUTPUT;
    emeans = zeros(size(tmeans));
    for i=1:height(ord)
        emeans(i) = nanmedian(table.(ey)(strcmp(table.(x), ord.(x){i})));
    end
    hold on;
    h_tm = plot(tmeans, 'r.', 'MarkerSize',20);
    h_em = plot(emeans, 'gd', 'MarkerFaceColor','g');
    hold off;

    mdn = rowfun(@(a,b) [nanmedian(a) nanmedian(b)], table, 'GroupingVariable',x, 'InputVariables',{ty ey}, 'OutputVariableName','OUTPUT');
    meancorr = corrcoef(mdn.OUTPUT, 'rows','complete');
    meancorr = meancorr(1,2);

    if isempty(xl)
        set(gca,'xticklabel',{' '})
    else
        xlabel(xl);
    end
    if ~isempty(yl)
        ylabel(yl);
    end
    if ~isempty(ti)
        title(sprintf(ti, meancorr));
    end

    boxes = findobj(gca, 'Tag','Box');
    legend([boxes(1) h_tm h_em], 'Turker ratings', 'Turker mean', 'Experimenter mean');
    
    texts = findobj(gca, 'Type','text');
    for t=1:length(texts)
        pos = get(texts(t), 'Position');
        pos(2) = pos(2) + 100;
        set(texts(t), 'Position',pos, 'FontSize',9, 'HorizontalAlignment','right');
    end

end
