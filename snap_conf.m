function snap_conf(slug)

    clf;
    
    N = length(dir(sprintf('~/Downloads/%s_*_test_conf.csv', slug)));
    
    for i=0:5000:(5000*(N-1))
        subplot(2,N+1, i/5000+1);
        do_plot(i, slug, '');
        subplot(2,N+1, i/5000+N+2);
        do_plot(i, slug, '_test');
    end

end

function do_plot(i, slug, suffix)

    conf = table2array(readtable(sprintf('~/Downloads/%s_%d%s_conf.csv', slug, i, suffix)));
    confplot(conf);
    title(sprintf('%.2f%%', nansum(diag(conf)/sum(sum(conf)))*100));
    
end
