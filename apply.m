function lambda = apply(func, n)
    alphabet = 'abcdefghijklmnopqrstuvwxyz';
    if n > length(alphabet)
        error('Too many inputs');
    end

    names = cell(1,n);
    items = cell(1,n);
    for i=1:n
        names{i} = alphabet(i);
        items{i} = ['feval(' func ',' names{i} ')'];
    end
    
    lambda = evalin('caller', ['@(' strjoin(names, ',') ') deal(' strjoin(items, ',') ')']);
end
