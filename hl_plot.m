function h = hl_plot(x, y, hl)
    h = plot(x, y);

    ax = axis;
    for t=1:size(hl,1)
        % https://stackoverflow.com/a/46249522/1114328
        px = [0 1 1 0]*(x(hl(t,2))-x(hl(t,1))) + x(hl(t,1));
        py = [0 0 1 1]*(ax(4)-ax(3)) + ax(3);
        h(end+1) = patch(px, py, 'k', 'LineStyle','none', 'FaceColor','k', 'FaceAlpha',0.25);
    end
end

