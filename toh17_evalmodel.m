function varargout = toh17_evalmodel(prop, labels, predictions)
    conf = confusionmat(labels, predictions);
    if prop(1) == 'h'
        cor = sum(diag(conf))/sum(sum(conf));
        [~,pv] = testcholdout(predictions, mode(labels)*ones(size(predictions)), labels);
        prec = nanmean(diag(conf)./sum(conf,2));
        rec = nanmean(diag(conf)'./sum(conf,1));
        err = mean(abs(labels - predictions'));
        varargout = {cor pv prec rec err};
    else
        [cor,pval] = corr(labels', predictions, 'Type','Spearman');
        fit = polyfit(labels, predictions', 1);

        slope = fit(1);
        fvu = mean((labels - predictions').^2)/var(labels);
        err = sqrt(mean((labels - predictions').^2));
        varargout = {cor slope pval fvu err};
    end
end

