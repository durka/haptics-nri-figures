function [d, taps] = segment_episode(d)

    % remove spikes
    [~, spikes] = findpeaks(abs(mean(d.int(:,2:4),2)), ...
                            'MinPeakHeight', 10*abs(mean(mean(d.int(:,2:4)))), ...
                            'MinPeakWidth', 1);
    
    d.int = smooth(d.int, 2:4, spikes);
    d.dt = smooth(d.dt, 1, spikes);

    % find taps
    [~, taps] = findpeaks(d.biws(:,4), ...
                          'MinPeakProminence', 8, ...
                          'MaxPeakWidth', 1000, ...
                          'MinPeakDistance', 1000);

end

function signal = smooth(signal, idx, spikes)

    for c=spikes'
        signal(c-10:c+10,idx) = interp1([c-10 c+10], signal([c-10 c+10],idx), c-10:c+10);
    end
    
end
