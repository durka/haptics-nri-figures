%% load

addpath(genpath('libsvm'));

CROPDIR = '/mnt/white/proton_data/crops/all_crops';
imset = imageSet([CROPDIR '/train_images'], 'recursive');
imtestset = imageSet([CROPDIR '/test_images'], 'recursive');
props = readtable([CROPDIR '/train_props.csv']);
testprops = readtable([CROPDIR '/test_props.csv']);

%% learn features

bag = bagOfFeatures(imset, 'VocabularySize',100);

%% classify

cls = trainImageCategoryClassifier(imset, bag);
evaluate(cls, imset);

%% prepare to regress

X = zeros(sum([imset.Count]), bag.VocabularySize);
Y = table2array(props(:,3:end));

for i=1:height(props)
    parts = strsplit(props(i,:).image{1}, '/');
    fprintf('%d/%d %s %s\n', i, height(props), props(i,:).category{1}, parts{end});
    img = imread(fullfile([CROPDIR '/train_images'], props(i,:).category{1}, parts{end}));
    X(i,:) = encode(bag, img);
end

testX = zeros(sum([imtestset.Count]), bag.VocabularySize);
testY = table2array(testprops(:,3:end));

for i=1:height(testprops)
    parts = strsplit(testprops(i,:).image{1}, '/');
    fprintf('%d/%d %s %s\n', i, height(testprops), testprops(i,:).category{1}, parts{end});
    img = imread(fullfile([CROPDIR '/test_images'], testprops(i,:).category{1}, parts{end}));
    testX(i,:) = encode(bag, img);
end

%% normalize data

train_mean = mean(X);
train_std = std(X);

X = bsxfun(@rdivide, bsxfun(@minus, X, train_mean), train_std);
testX = bsxfun(@rdivide, bsxfun(@minus, testX, train_mean), train_std);

%% regress

mdl = cell(1,size(Y,2));
for i=1:size(Y,2)
    fprintf('Fitting %d...\n', i);
    %mdl{i} = fitlm(X, Y(:,i));
    mdl{i} = svmtrain(Y(:,i), X, '-m 1000 -s 4 -t 2 -q -g 0.05 -n 0.75');
end

%% evaluate regression

fprintf('\n\nTraining error\n\n');
predY = zeros(size(Y));
for i=1:size(Y,2)
    fprintf('Evaluating %s... ', props.Properties.VariableNames{2+i});
    %predY(:,i) = predict(mdl{i}, X);
    predY(:,i) = svmpredict(zeros(size(predY(:,i))), X, mdl{i}, '-q');
    fprintf('RMS pct err = %g%%\n', 100*sqrt(mean(((predY(:,i) - Y(:,i))./Y(:,i)).^2)));
end

fprintf('\n\nTest error\n\n');
testpredY = zeros(size(testY));
for i=1:size(Y,2)
    fprintf('Evaluating %s... ', props.Properties.VariableNames{2+i});
    %testpredY(:,i) = predict(mdl{i}, testX);
    testpredY(:,i) = svmpredict(zeros(size(testpredY(:,i))), testX, mdl{i}, '-q');
    fprintf('RMS pct err = %g%%\n', 100*sqrt(mean(((testpredY(:,i) - testY(:,i))./testY(:,i)).^2)));
end

%% evaluate a random image

idx = randi(size(testX,1));

subplot(1,3,1);
parts = strsplit(testprops(idx,:).image{1}, '/');
imshow(imread(fullfile([CROPDIR '/test_images'], testprops(idx,:).category{1}, parts{end})));
title(sprintf('%s %s', testprops(idx,:).category{1}, parts{end}), 'interpreter','none');

subplot(1,3,2);
plot(testX(idx,:));

subplot(1,3,3);
pred = zeros(size(testY(idx,:)));
for i=1:length(pred)
    %pred(i) = predict(mdl{i}, testX(idx,:));
    pred(i) = svmpredict(zeros(size(pred(i))), testX(idx,:), mdl{i}, '-q');
end
plot([testY(idx,:); pred; (pred - testY(idx,:))./testY(idx,:)]', '.-', 'markersize',20);
set(gca, 'XTick', 1:size(Y,2));
set(gca, 'XTickLabel', props.Properties.VariableNames(3:end));

%% comparisons

figure
for i=1:8
    subplot(4,2,i)
    plot([Y(:,i) predY(:,i)])
    axis tight
    title(props.Properties.VariableNames{2+i})
end
suplabel('TRAIN', 't');

figure
for i=1:8
    subplot(4,2,i)
    plot([testY(:,i) testpredY(:,i)])
    axis tight
    title(props.Properties.VariableNames{2+i})
end
suplabel('TEST', 't');
