function toh17_plotmodel(x, y, prop, suffix)
    if prop(1) == 'h'
        C = confusionmat(x, y);
        imagesc(C./sum(C,2));
        colormap(1-gray);
    else
        coeff = polyfit(x, y, 1);
        hscatt = plot(x, y, '.', 'markersize',12);
        hold on;
        htrend = plot(x, coeff(1)*x + coeff(2), 'linewidth',3);
        hideal = plot(x, x, 'linewidth',3);
        hold off;
        set(gca, 'FontSize',30, 'Children',[htrend hideal hscatt]);
    end
    print('-dpng', '-r100', sprintf('prop_%s_%s.png', prop, suffix));
end

