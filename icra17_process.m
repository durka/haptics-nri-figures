function data = icra17_process(mode, data, varargin)

    materials = data.keys;

    switch mode
        case 'vicon'
            data = vicon(data, materials, varargin{:});
            
        case 'bluefox'
            data = bluefox(data, materials, varargin{:});
            
        case 'both'
            data = vicon(data, materials, varargin{:});
            data = bluefox(data, materials, varargin{:});
    end
end

function data = vicon(data, materials, masscom, H_vic2bod, H_m402bod, H_bal2imu)
    for m = 1:length(materials)
        ep = data(materials{m});
        
        if nargin > 2
            if ~isstruct(masscom)
                masscom = struct('mass', masscom, 'com', [0;0;0]);
            end
            [~,~,~,~,~,~, vei, ai, ~,~,~, iws] = process_stick(ep.v, ep.int, ep.acc, masscom.mass, masscom.com, H_vic2bod, H_m402bod, H_bal2imu, -ep.off);
            ep.vei = vei;
            ep.ai = ai;
            ep.iws = iws;
        end

        data(materials{m}) = ep;
    end
end

function data = bluefox(data, materials, masscom, H_vic2bod, H_m402bod, H_bal2imu)
    for m = 1:length(materials)
        ep = data(materials{m});

        % remove spikes
        w = warning('off', 'signal:findpeaks:largeMinPeakHeight');
        [~, spikes] = findpeaks([abs(mean(ep.int(:,2:4),2)); 0], ...
                                'MinPeakHeight', 100);
        warning(w);
        
        ep.spikes = spikes;
        ep.int = icra17_process_smooth(ep.int, 2:4, spikes);
        ep.acc = icra17_process_smooth(ep.acc, 2:7, spikes);
        ep.imu.mic = icra17_process_smooth(ep.imu.mic, 2, spikes);
        ep.dt = icra17_process_smooth(ep.dt, 1, spikes);
        ep.imu.mic = icra17_process_smooth(ep.imu.mic, 2, spikes);

        if nargin > 2
            if ~isstruct(masscom)
                masscom = struct('mass', masscom, 'com', [0;0;0]);
            end
            [~,~,~,~,~,~, vei, ai, ~,~,~, iws] = process_stick(ep.motrak, ep.int, ep.acc, masscom.mass, masscom.com, H_vic2bod, H_m402bod, H_bal2imu);
            ep.bvei = vei;
            ep.bai = ai;
            ep.biws = iws;
            ep.imu.mic = ep.imu.mic((ep.imu.mic(:,1) >= ep.bvei(1,1)) & (ep.imu.mic(:,1) <= ep.bvei(end,1)), :);
        end

        % find taps
        ep = icra17_process_taps(ep);

        data(materials{m}) = ep;
    end
end
