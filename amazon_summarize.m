% cropdir: dir containing date dirs
% dates: array of dates to load from
% property:
%   - 'none' -- only load the data and filter bad ratings
%   - 'hard'/'rough'/'sticky'/'warm' -- plot ratings of single property
%   - 'corr' -- measure and plot correlation between experimenter/turker ratings
%   - 'pics' -- plot grid of surface pictures
%   - 'compare' -- comparison plot between random subsets of Turkers (subsets are stored as axes properties)
%   - 'compare_persistent' -- same as 'compare' but read subsets from previous 'compare' figure
% exclude (optional): cell array of YYYYMMDD-eff-num episodes to ignore
% props (optional): cell array of (cell array of YYYYMMDD-eff-num, matrix of numerical properties with rows in same order, feature names)
function [data, attn, badimg] = amazon_summarize(cropdir, dates, property, exclude, props)

    if nargin < 5
        props = [];
        if nargin < 4
            exclude = {};
        end
    end

    % load data

    data = [];
    for d=1:length(dates)
        data = [data; readtable(fullfile(cropdir, num2str(dates(d)), 'amazon_output_cooked.csv'))];
    end
    
    fprintf('(before filtering) %d ratings of %d images of %d surfaces\n', height(data), length(unique(sort(data.hit_id))), length(unique(sort(data.surface))));
    
    % filter out bad lines
    [data, rm1] = filter_table(data, @(d) strcmp(d.turk_shape, 'wide'), ...
                                     @(d) d.img_width <= d.img_height);
    [data, rm2] = filter_table(data, @(d) strcmp(d.turk_shape, 'tall'), ...
                                     @(d) d.img_width >= d.img_height);
    [data, rm3] = filter_table(data, @(d) strcmp(d.turk_shape, 'square'), ...
                                     @(d) ((d.img_width ./ d.img_height) >= 4/3) | ((d.img_width ./ d.img_height) <= 3/4)); % FIXME are these the right limits?
    [data, rm4] = filter_table(data, @(d) strcmp(d.turk_quality, 'bad'));
    for e=1:length(exclude)
        data    = filter_table(data, @(d) strcmp(d.source, exclude{e}));
    end
    attn = [rm1; rm2; rm3];
    badimg = rm4;

    % print stats

    fprintf('(after filtering) %d ratings of %d images of %d surfaces\n', height(data), length(unique(sort(data.hit_id))), length(unique(sort(data.surface))));

    % plot results

    switch property
        case {'hard', 'rough', 'sticky', 'warm'}
            clf;
            ness = [upper(property(1)) property(2:end) 'ness'];
            meancorr = rowfun_boxplot(data, property, 'surface', ...
                ['Turker Ratings of ' ness ' (corr=%g)'], ...
                'Surface', ness);
            fprintf('Mean correlation coefficient = %.2f\n', meancorr);
        case 'corr'
            clf;
            do_corr('exp', data, 141);
            do_corr('turker', data, 142);
            do_corr('both', data, 143);

            if ~isempty(props)
                for i=1:height(data)
                    j = find(strcmp(props{1}, data.source{i}), 1);
                    if isempty(strfind(data.source{i}, 'stick'))
                        data.props{i} = nan([1 size(props{2},2)]);
                    elseif isempty(j)
                        fprintf('no properties for %s :(\n', data.source{i});
                        data.props{i} = nan([1 size(props{2},2)]);
                    else
                        data.props{i} = props{2}(j,:);
                    end
                end
                do_corr('props', data, 144, props{3});
            end
        case 'pics'
            cropnums = rowfun(@(d,r) [d(1) r(1)], data, 'GroupingVariable','surface', 'InputVariables',{'date' 'crop_num'}, 'OutputVariableName','OUTPUT');
            idx = randperm(height(cropnums), 10);
            images = {};
            names = {};
            for i=idx
                if i > height(cropnums)
                    break
                end
                images = [images; fullfile(cropdir, num2str(cropnums.OUTPUT(i,1)), sprintf('crop%d.png', cropnums.OUTPUT(i,2)))];
                names = [names; cropnums.surface(i)];
            end
            fprintf('Tiling... ');
            system(sprintf('gm montage -geometry 480x480+2+16 -pointsize 40 -tile 5x2 %s amazon_pics.png', strjoin(cellfun(@(n,f) sprintf('-label ''%s'' %s', n, f), names, images, 'UniformOutput',false), ' ')));
            fprintf('wrote amazon_pics.png\n');
        case {'compare', 'compare_persistent'}
            switch property
                case 'compare'
                    workers = unique(data.worker_id);
                    idx_a = logical(randi(2, [length(workers) 1])-1); % random ~half subset
                    idx_b = ~idx_a;
                    workers_a = workers(idx_a);
                    workers_b = workers(idx_b);
                    a = ismember(data.worker_id, workers_a);
                    b = ismember(data.worker_id, workers_b);
                case 'compare_persistent'
                    a = subsref(get(subplot(131), 'UserData'), substruct('()', {'rows'}));
                    b = subsref(get(subplot(132), 'UserData'), substruct('()', {'rows'}));
                    workers_a = unique(table2cell(data(a,'worker_id')));
                    workers_b = unique(table2cell(data(b,'worker_id')));
            end
            
            clf;
            [ac, ad] = amazon_summarize_do_corr('both', data(a,:), 131);
            title({sprintf('Subset A (%d workers, %d answers)', length(workers_a), nnz(a)), ''});
            ax = gca;
            ax.UserData = containers.Map;
            ax.UserData('rows') = a;
            [bc, bd] = amazon_summarize_do_corr('both', data(b,:), 132);
            title({sprintf('Subset B (%d workers, %d answers)', length(workers_b), nnz(b)), ''});
            ax = gca;
            ax.UserData = containers.Map;
            ax.UserData('rows') = b;
            
            n = {'hard' 'rough' 'sticky' 'warm'};
            nn = [cellfun(@(s) ['A' s], n, 'UniformOutput',false) cellfun(@(s) ['B' s], n, 'UniformOutput',false)];
            amazon_summarize_conf(133, corrfunc([ad(:,5:8) bd(:,5:8)]), 'A vs B', nn);
            line(xlim, [4.51 4.51], 'Color','k', 'LineWidth',0.5);
            line([4.5 4.5], ylim, 'Color','k', 'LineWidth',0.5);
    end
end

function out = corrfunc(in)
    N = size(in,2);
    out = zeros(N,N);
    for i=1:N
        for j=1:N
            notnan = in(~isnan(in(:,i)) & ~isnan(in(:,j)), [i j]);
            if isempty(notnan)
                out(i,j) = 0;
            else
                out(i,j) = corr(notnan(:,1), notnan(:,2));
            end;
            %out(i,j) = signrank(notnan(:,1), notnan(:,2), 'alpha',0.01);
        end
    end
end

function m = nanmedian_robust(v) %#ok<DEFNU>
    if iscell(v)
        m = nanmean(cell2mat(v), 1);
    else
        v(isnan(v)) = [];
        v(abs(v - mean(v)) > 2*std(v)) = [];
        m = median(v);
    end
end

function [c, d] = do_corr(mode, data, sp, lbls)
    if nargin < 3
        % no subplot
        figure;
    else
        subplot(sp);
    end
    
    n = {'hard' 'rough' 'sticky' 'warm'};
    nn = [cellfun(@(s) ['exp_' s], n, 'UniformOutput',false) cellfun(@(s) ['turk_' s], n, 'UniformOutput',false)];
    if strcmp(mode, 'props')
        nn{end+1} = 'props';
    end
    nonbio_idx = cellfun(@(s) isempty(strfind(s, 'bio')), data.source);
    data(nonbio_idx, 'exp_sticky') = table(nan(size(data(nonbio_idx, 'exp_sticky'))));
    data = rowfun(apply('@(v) nanmedian_robust(v)', length(nn)), data, 'GroupingVariable','surface', 'InputVariables',nn, 'OutputVariableNames',nn);
    nn = [cellfun(@(s) ['E' s], n, 'UniformOutput',false) cellfun(@(s) ['T' s], n, 'UniformOutput',false)];
    
    ed = [data.exp_hard data.exp_rough data.exp_sticky data.exp_warm];
    td = [data.turk_hard data.turk_rough data.turk_sticky data.turk_warm];
    
    switch mode
        case 'exp'
            fprintf('\nExperimenter:\n');
            ec = corrfunc(ed);
            disp(table(ec(:,1), ec(:,2), ec(:,3), ec(:,4), 'VariableNames',n, 'RowNames',n));
            conf(sp, ec, 'Experimenter', n);
            c = ec; d = ed;
        case 'turker'
            fprintf('Turkers:\n');
            tc = corrfunc(td);
            disp(table(tc(:,1), tc(:,2), tc(:,3), tc(:,4), 'VariableNames',n, 'RowNames',n));
            conf(sp, tc, 'Turkers', n);
            c = tc; d = td;
        case 'both'
            fprintf('Cross:\n');
            cc = corrfunc([ed td]);
            disp(table(cc(:,1), cc(:,2), cc(:,3), cc(:,4), cc(:,5), cc(:,6), cc(:,7), cc(:,8), 'VariableNames',nn, 'RowNames',nn));
            conf(sp, cc, 'Cross', [n n]);
            line(xlim, [4.51 4.51], 'Color','k', 'LineWidth',0.5);
            line([4.5 4.5], ylim, 'Color','k', 'LineWidth',0.5);
            c = cc; d = [ed td];
        case 'props'
            pd = data.props;
            pc = nan([size(ed,2)+size(td,2) size(pd,2)]);
            d = [ed td];
            for x=1:size(d,2)
                for y=1:size(pd,2)
                    corr = corrfunc([d(:,x) pd(:,y)]);
                    pc(x,y) = corr(1,2); % only cross-corr
                end
                rsq = crossval(@(Xtr,Ytr,Xt,Yt) rsquared(predict(fitlm(Xtr, Ytr, 'linear'), Xt), Yt), pd, d(:,x), 'kfold',3);
                mdl = fitlm(pd, d(:,x), 'linear', 'RobustOpts','on');
                fprintf('d(:,%d) fits with R^2=%g %s\n', x, mean(rsq), mat2str(mdl.Coefficients.Estimate',3));
            end
            conf(sp, pc, 'Props', [n n], lbls);
            line(xlim, [4.51 4.51], 'Color','k', 'LineWidth',0.5);
            c = pc; d = [ed td];
            %keyboard;
    end
end

% https://en.wikipedia.org/wiki/Coefficient_of_determination#Definitions
function R2 = rsquared(f, y)
    ybar = nanmean(y);
    SStot = nansum((y - ybar).^2);
    SSres = nansum((y - f).^2);
    R2 = 1 - SSres/SStot;
end

function conf(sp, c, t, n, m)
    if nargin < 5
        m = n;
    end
    subplot(sp);
    cla;
    imagesc(abs(c), [0 1]);
    colormap(flipud(gray));
    for i=1:size(c,1)
        for j=1:size(c,2)
            if abs(c(i,j)-1) > 1e-5
                text(j,i, sprintf('%.2f', c(i,j)), 'Color',(abs(c(i,j))>0.5)*[1 1 1] + (abs(c(i,j))<=0.5)*[0 0 0], 'FontWeight','bold', 'HorizontalAlignment','center');
            end
        end
    end
    
    axis equal tight;
    set(gca, 'XTick', 1:length(m));
    set(gca, 'XTickLabel', m);
    set(gca, 'YTick', 1:length(n));
    set(gca, 'YTickLabel', n);
    
    % copy axis labels
    ax = gca;
    axes('Position',ax.Position, 'Color','none', ...
         'XAxisLocation','top', 'YAxisLocation','right', ...
         'XLim',ax.XLim, 'YLim',ax.YLim, ...
         'DataAspectRatio',ax.DataAspectRatio, ...
         'XTick',ax.XTick, 'YTick',ax.YTick, ...
         'XTickLabel',ax.XTickLabel, 'YTickLabel',flipud(ax.YTickLabel));

    set(gcf, 'CurrentAxes',ax);
    title({t, ''});
end

