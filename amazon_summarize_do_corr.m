function [c, d] = amazon_summarize_do_corr(mode, data, sp)
    if nargin < 3
        % no subplot
        figure;
    else
        subplot(sp);
    end
    
    n = {'hard' 'rough' 'sticky' 'warm'};
    nn = [cellfun(@(s) ['exp_' s], n, 'UniformOutput',false) cellfun(@(s) ['turk_' s], n, 'UniformOutput',false)];
    data = rowfun(apply('@(v) nanmedian_robust(v)', 8), data, 'GroupingVariable','surface', 'InputVariables',nn, 'OutputVariableNames',nn);
    nn = [cellfun(@(s) ['E' s], n, 'UniformOutput',false) cellfun(@(s) ['T' s], n, 'UniformOutput',false)];
    
    ed = [data.exp_hard data.exp_rough data.exp_sticky data.exp_warm];
    td = [data.turk_hard data.turk_rough data.turk_sticky data.turk_warm];
    
    switch mode
        case 'exp'
            fprintf('\nExperimenter:\n');
            ec = corrfunc(ed);
            disp(table(ec(:,1), ec(:,2), ec(:,3), ec(:,4), 'VariableNames',n, 'RowNames',n));
            conf(sp, ec, 'Experimenter', n);
            c = ec; d = ed;
        case 'turker'
            fprintf('Turkers:\n');
            tc = corrfunc(td);
            disp(table(tc(:,1), tc(:,2), tc(:,3), tc(:,4), 'VariableNames',n, 'RowNames',n));
            conf(sp, tc, 'Turkers', n);
            c = tc; d = td;
        case 'both'
            fprintf('Cross:\n');
            cc = corrfunc([ed td]);
            disp(table(cc(:,1), cc(:,2), cc(:,3), cc(:,4), cc(:,5), cc(:,6), cc(:,7), cc(:,8), 'VariableNames',nn, 'RowNames',nn));
            conf(sp, cc, 'Cross', nn);
            line(xlim, [4.51 4.51], 'Color','k', 'LineWidth',0.5);
            line([4.5 4.5], ylim, 'Color','k', 'LineWidth',0.5);
            c = cc; d = [ed td];
    end
end


