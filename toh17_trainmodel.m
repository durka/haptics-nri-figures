function [trains, tests, cols, mod, Y, Yt] = toh17_trainmodel(X, Xt, trains, tests, cols, prop, gsp)
    trains = table2array(trains)';
    tests = table2array(tests)';
    nn = ~isnan(trains);
    nnt = ~isnan(tests);
    trains = trains(nn);
    tests = tests(nnt);
    if prop(1) ~= 'h'
        trainmean = mean(trains);
        trainstd = std(trains);
        trains = (trains - trainmean)/trainstd;
        tests = (tests - trainmean)/trainstd;
    end
    X = X(nn,:);
    Xt = Xt(nnt,:);
    trm = mean(X);
    trs = std(X);
    trs(trs==0) = 1;
    X = bsxfun(@rdivide, bsxfun(@minus, X, trm), trs);
    Xt = bsxfun(@rdivide, bsxfun(@minus, Xt, trm), trs);
    if false && isempty(cols)
        fprintf('f');
        warning('off', 'stats:LinearModel:RankDefDesignMat');
        cols = sequentialfs(@(x,y) subsref(fitglm(x, y), substruct('.', 'Deviance')), X,trains', 'cv','none', 'nullmodel',true, 'direction','forward', 'options', statset('display','off'), 'nfeatures',60);
        warning('on', 'stats:LinearModel:RankDefDesignMat');
    end
    X = X;%(:,cols);
    Xt = Xt;%(:,cols);
    if prop(1) == 'h'
        mod = svmtrain(trains', X, sprintf('-m 1000 -s 0 -t 2 -c %g -g %g -q', gsp(1), gsp(2)));
    else
        mod = svmtrain(trains', X, sprintf('-m 1000 -s 4 -t 2 -n %g -g %g -q', gsp(1), gsp(2)));
    end
    Y = svmpredict(trains', X, mod, '-q');
    Yt = svmpredict(tests', Xt, mod, '-q');

end

