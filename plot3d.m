function plot3d(xyz, label, shortlabel)

    if nargin == 2
        shortlabel = label;
    end
    x = xyz(:,1);
    y = xyz(:,2);
    z = xyz(:,3);
    t = 1:length(x);
    
    % 3D view
    subplot(221);
    cline(x, y, z, t);
    view(3);
    axis equal vis3d;
    grid on;
    xlabel(shortlabel{1});
    ylabel(shortlabel{2});
    zlabel(shortlabel{3});
    
    % top view
    subplot(222)
    cline(x, y, [], t);
    axis equal;
    grid on;
    xlabel(label{1});
    ylabel(label{2});
    
    % side 1
    subplot(223);
    cline(x, z, [], t);
    axis equal;
    ax = axis;
    grid on;
    xlabel(label{1});
    ylabel(label{3});
    
    % side 2
    subplot(224);
    cline(y, z, [], t);
    maxis([3 4], ax([3 4]));
    axis square;
    grid on;
    xlabel(label{2});
    ylabel(label{3});
    
end
