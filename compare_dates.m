function compare_dates(dat1, dat2, lbl1, lbl2)

    mat1 = dat1.keys;
    mat2 = dat2.keys;

    for i=1:length(mat1)
        fprintf('%d/%d %s ', i, length(mat1), mat1{i});

        try
            d = dat1(mat1{i});
            e = dat2(mat1{i});
            fprintf('\x2713\n');

            clf;
            
            r = 5;
            if ~isempty(d.sensor.opto) || ~isempty(d.sensor.bio)
                r = 6;
            end
            
            subplot(r,2,1);
            imshow(subsref(strsplit(ls(fullfile(d.datadir, d.date, d.flow, d.i, 'crops', '*_crop.png')), '\n'), substruct('{}', {1})));
            subplot(r,2,2);
            imshow(subsref(strsplit(ls(fullfile(e.datadir, e.date, e.flow, e.i, 'crops', '*_crop.png')), '\n'), substruct('{}', {1})));
            
            compare_plot(r,2,3, d.bvei, e.bvei, 2:4, 'Position');
            compare_plot(r,2,5, d.biws, e.biws, 2:4, 'Force');
            compare_plot(r,2,7, d.bai, e.bai, 2:4, 'Vibration');
            compare_plot(r,2,9, d.imu.mic, e.imu.mic, 2, 'Sound');
            
            if ~isempty(d.sensor.opto)
                compare_plot(r,2,11, d.sensor.opto, e.sensor.opto, 2:4, 'Optoforce');
            elseif ~isempty(d.sensor.bio)
                compare_plot(r,2,11, d.sensor.bio, e.sensor.bio, [2 25:45], 'Biotac');
            end

            suplabel(sprintf('        %s        \n%s            %s', mat1{i}, lbl1, lbl2), 't');
            pause;
        catch err
            if strcmp(err.identifier, 'MATLAB:Containers:Map:NoKey')
                fprintf('\x2717\n');
            else
                rethrow(err);
            end
        end
    end

end

function compare_plot(w,h,i, arr1, arr2, cols, lbl)
    subplot(w,h,i);
    plot(arr1(:,1)-arr1(1,1), arr1(:,cols));
    axis tight;
    ylabel(lbl);
    
    subplot(w,h,i+1);
    plot(arr2(:,1)-arr2(1,1), arr2(:,cols));
    axis tight;
end
