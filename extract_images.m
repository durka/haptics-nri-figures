function [pts, fa, bbox] = extract_images(d, fa)
    bt = readtable(fullfile(d.datadir, d.date, d.flow, num2str(d.i), 'bluefox/bluefox_times.csv'));

    %% find flat part
    half = round(size(d.bvei,1)/2);
    onethird = round(size(d.bvei,1)/3);
    twothirds = round(2*size(d.bvei,1)/3);
    start = onethird;
    stop = twothirds;
    %z_level = mean(d.bvei(onethird:twothirds, 4)); % mean Z-coord of middle 1/3
    %start = find(d.bvei(1:half,4)   > z_level+2, 1, 'last')  + 500;
    %stop  = find(d.bvei(half:end,4) > z_level+2, 1, 'first') - 500 + half;

    if nargin == 1
        % get frame num with lowest endeff y-coord
        [~,tas] = sort(d.bvei(start:stop,3));
        for i=1:length(tas)
            ta = tas(i) + start;
            [~,fa] = min(abs(d.bvei(ta,1) - bt.UnixTimestamp));
            fa = bt.FrameNumber(fa);
            if length(d.april(fa).ids) > 20
                break;
            end
        end
    end

    imga = imread(fullfile(d.datadir, d.date, d.flow, num2str(d.i), sprintf('bluefox/bluefox%d.png', fa)));

    rect = [655 704 273 496];
    pt = [778 760];
    %imga(rect(4):rect(2), rect(1):rect(3), :) = 0;

    %%

    pts = [];

    apr = d.april;
    apra = apr(fa);
    for fi=sort(cell2mat(apr.keys))
        [~,ti] = min(abs(d.bvei(:,1) - bt.UnixTimestamp(bt.FrameNumber == fi)));
        if ti < start || ti > stop
            continue;
        end
        aprb = apr(fi);
        if length(aprb.ids) > 20
            [~,ia,ib] = intersect(apra.ids, aprb.ids);
            if length(ia) >= 4
                ctra = apra.centers(:,ia)';
                ctrb = aprb.centers(:,ib)';
                H = fitgeotrans(ctrb, ctra, 'similarity');

                ptb = [pt 1] * H.T;
                if ptb > 0
                    pts(end+1,:) = ptb(1:2);
                end
            end
        end
    end

    bbox = round([min(pts(:,1))-25 max(pts(:,2))+25 max(pts(:,1))+25 min(pts(:,2))-25]);
    bbox(4) = max(bbox(4), 1);
    bbox(2) = min(bbox(2), size(imga,1));
    bbox(1) = max(bbox(1), 1);
    bbox(3) = min(bbox(3), size(imga,2));

    if (bbox(3) - bbox(1)) / (bbox(2) - bbox(4)) < 0.3 % narrow bbox
        % special case for early datasets
        tag32 = find(apra.ids == 32);
        tag40 = find(apra.ids == 40);
        if ~isempty(tag32) && ~isempty(tag40)
            bottom_of_frame = mean(apra.centers(2,[tag32 tag40]));
            if bottom_of_frame < 800
                % make bbox square
                bbox_hcenter = mean(bbox([1 3]));
                bbox_height = bbox(2) - bbox(4);
                bbox(1) = round(bbox_hcenter - bbox_height/2);
                bbox(3) = round(bbox_hcenter + bbox_height/2);
            end
        end
    end

    fprintf('BBOX: [%d %d %d %d] (%g)\n', bbox(1), bbox(2), bbox(3), bbox(4), rectint(rect, [bbox(1) bbox(4) bbox(3)-bbox(1) bbox(2)-bbox(4)])/((bbox(3)-bbox(1))*(bbox(2)-bbox(4))));
    imwrite(imga(bbox(4):bbox(2), bbox(1):bbox(3), :), fullfile(d.datadir, d.date, d.flow, num2str(d.i), 'surface.png'));

    try
        rmdir(fullfile(d.datadir, d.date, d.flow, num2str(d.i), 'crops'), 's');
    catch
        % pass
    end
    mkdir(fullfile(d.datadir, d.date, d.flow, num2str(d.i), 'crops'));
    for tb=start:3000:stop
        [~,fb] = min(abs(d.bvei(tb,1) - bt.UnixTimestamp));
        fb = bt.FrameNumber(fb);
        imgb = imread(fullfile(d.datadir, d.date, d.flow, num2str(d.i), sprintf('bluefox/bluefox%d.png', fb)));
        aprb = apr(fb);
        if length(aprb.ids) > 20
            [~,ia,ib] = intersect(apra.ids, aprb.ids);
            if length(ia) >= 4
                ctra = apra.centers(:,ia)';
                ctrb = aprb.centers(:,ib)';
                H = fitgeotrans(ctrb, ctra, 'similarity');

                ptsb = [pts ones(size(pts,1),1)] / H.T;
                bboxb = round([min(ptsb(:,1))-25 max(ptsb(:,2))+25 max(ptsb(:,1))+25 min(ptsb(:,2))-25]);
                bboxb(4) = max(bboxb(4), 1);
                bboxb(2) = min(bboxb(2), size(imgb,1));
                bboxb(1) = max(bboxb(1), 1);
                bboxb(3) = min(bboxb(3), size(imgb,2));
                frac = rectint(rect, [bboxb(1) bboxb(4) bboxb(3)-bboxb(1) bboxb(2)-bboxb(4)])/((bboxb(3)-bboxb(1))*(bboxb(2)-bboxb(4)));
                fprintf('%d bbox=%s (%g)\n', tb, mat2str(bboxb), frac);

                if frac < 0.25 && bboxb(2) > bboxb(4) && bboxb(3) > bboxb(1)
                    april_render(d, 'path', fb);
                    print('-dpng', fullfile(d.datadir, d.date, d.flow, num2str(d.i), 'crops', sprintf('%d_frame.png', tb)));
                    imwrite(imgb(bboxb(4):bboxb(2), bboxb(1):bboxb(3), :), fullfile(d.datadir, d.date, d.flow, num2str(d.i), 'crops', sprintf('%d_crop.png', tb)));
                end
            end
        end
    end
end

