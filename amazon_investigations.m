%% load data

[data, attn, badimg] = amazon_summarize('../nri/crops', [20171106 20171109 20171117], 'none');

%% check on workers who got the attention-check questions wrong

all_workers = unique([data.worker_id; attn.worker_id; badimg.worker_id]);
attn_workers = unique(attn.worker_id);

for w=1:length(attn_workers)
    n_good = nnz(strcmp(data.worker_id, attn_workers{w}));
    n_bad = nnz(strcmp(attn.worker_id, attn_workers{w}));
    
    if n_good > 0
        fprintf('Worker %s: %d good, %d bad\t\t%.1f%%\n', attn_workers{w}, n_good, n_bad, n_good/(n_good+n_bad)*100);
    end
end

% CONCLUSIONS
%   - worker A1F2QQ8D4373H1 is insane and I'm a bit worried they are
%     dominating the entire dataset (should've limited participation somehow)
%   - no worker fell below 50% correct attention checks
%   - based on those two I'm going to ignore the issue for now

%% look into images rated as bad

goodimg = data(strcmp(data.turk_quality, 'good'), :);
okimg = data(strcmp(data.turk_quality, 'ok'), :);

gbs = rowfun(@(s,h) unique(h), goodimg, 'InputVariables',{'surface','hit_id'}, 'GroupingVariable','surface', 'OutputVariableNames',{'hit_id'});
obs = rowfun(@(s,h) unique(h), okimg, 'InputVariables',{'surface','hit_id'}, 'GroupingVariable','surface', 'OutputVariableNames',{'hit_id'});
bbs = rowfun(@(s,h) unique(h), badimg, 'InputVariables',{'surface','hit_id'}, 'GroupingVariable','surface', 'OutputVariableNames',{'hit_id'});

surfaces = unique([gbs.surface; obs.surface; bbs.surface]);
hits = unique([gbs.hit_id; obs.hit_id; bbs.hit_id]);

% look for HITs where a majority of turkers think the image was bad
problem = table([],[],[],[], 'VariableNames',{'hit_id','source','n_good','n_bad'});
for h=1:length(hits)
    n_good = nnz(strcmp(goodimg.hit_id, hits{h})) + nnz(strcmp(okimg.crop_num, hits{h}));
    n_bad = nnz(strcmp(badimg.hit_id, hits{h}));
    
    if n_bad > 0 && n_bad > n_good
        source = table2array(unique(data(strcmp(data.hit_id, hits{h}), 'source')));
        problem = [problem; {hits{h}, source, n_good, n_bad}];
    end
end

% look for surfaces where a majority of crops where judged bad
sources = unique(problem.source);
exclude = {};
for s=1:length(sources)
    idx = strcmp(problem.source, sources{s});
    n_good = sum(table2array(problem(idx,'n_good')));
    n_bad = sum(table2array(problem(idx,'n_bad')));
    if nnz(idx) > 1 && n_bad > n_good
        fprintf('%s is bad (%d%% bad from %d viewers across %d crops)\n', sources{s}, round(n_bad/(n_good+n_bad)*100), n_good+n_bad, nnz(idx));
        exclude = [exclude sources{s}];
    end
end

%% reload data excluding identified exclusions

amz_data = amazon_summarize('../../nri/crops', [20171106 20171109 20171117], 'none', exclude);

amazon_summarize('../../nri/crops', [20171106 20171109 20171117], 'hard', exclude);
