function map = plot_rates(data)
    rates = zeros(0,2);
    labels = cell(0,0);
    map = containers.Map;

    materials = data.keys;

    for m=1:length(materials)
        d = data(materials{m});
        rate = 1/mean(diff(d.int(:,1)));
        rates = [rates; d.int(1,1) rate];
        labels = [labels d.i];
        map(sprintf('%d', round(rate))) = materials{m};
    end

    [~, idx] = sort(rates(:,1));
    rates = rates(idx,:);
    rates(:,1) = rates(:,1) - rates(1,1);
    plot(rates(:,1), rates(:,2), '.', 'MarkerSize',20);
    gca.XTickLabel = labels;
end

