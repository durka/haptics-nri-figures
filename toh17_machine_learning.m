%% setup

addpath('/home/haptics/proton/yang_icra2016/caffe-rc4/matlab')
addpath('matlab-lmdb');
addpath('libsvm/matlab');
caffe.set_mode_cpu

DATADIR = '/mnt/usbstick/proton_data';
MINCDIR = fullfile(DATADIR, 'minc_model');

%% load data

surfaces = readtable('surfaces.csv', ...
                     'Format', '%q%*q%q%q%q%q%q%q%q%*q%q%q%*[^\n]');

%% run MINC classification

minc_gnet = caffe.Net(fullfile(MINCDIR, 'deploy-googlenet.prototxt'), fullfile(MINCDIR, 'minc-googlenet.caffemodel'), 'test');
minc_materials = {'brick', 'carpet', 'ceramic', 'fabric', 'foliage', 'food', 'glass', 'hair', 'leather', 'metal', 'mirror', 'other', 'painted', 'paper', 'plastic', 'polishedstone', 'skin', 'sky', 'stone', 'tile', 'wallpaper', 'water', 'wood'};

errors = containers.Map;
N = size(glob(fullfile(DATADIR, '*', '*', '*', 'surface.png')), 1);
minc_output = struct('fname',cell(1,N), 'img',cell(1,N), 'z',cell(1,N), 'act',cell(1,N), 'ratings',cell(1,N));
idx = 1;
for m=1:length(minc_materials)
    testidx = strcmp(surfaces.MINCLabel, minc_materials{m});
    if nnz(testidx) > 0 && ~isempty(surfaces.StickDate{testidx})
        testidx = find(testidx);
        for ti=1:length(testidx)
            flows = {'stickcam' 'optocam' 'biocam'};
            for fi=1:length(flows)
                fname = fullfile(DATADIR, surfaces.StickDate{testidx(ti)}, flows{fi}, surfaces.StickNum{testidx(ti)}, 'surface.png');
                if exist(fname, 'file')
                    try
                        fprintf('%d/%d %s\n', idx, N, fname);

                        % metadata
                        flow = parse_flow(strrep(fname, 'surface.png', sprintf('%s.flow', flows{fi})));
                        ratings = flow.answers;

                        % network prediction
                        img = toh17_prepare_image(fname, [104 117 124], 224);
                        z = minc_gnet.forward({img});
                        z = mean(z{1}, 2);
                        act = mean(squeeze(minc_gnet.blobs('pool5/7x7_s1').get_data), 2);

                        % save to struct array
                        minc_output(idx).fname = fname;
                        minc_output(idx).img = img;
                        minc_output(idx).z = z;
                        minc_output(idx).act = act;
                        minc_output(idx).ratings = ratings;
                    catch err
                        fprintf('ERROR: %s\n', err.message);
                        disp(getReport(err));
                        errors(sprintf('m=%d ti=%d fi=%d', m, ti, fi)) = err;
                    end
                    idx = idx + 1;
                end
            end
        end
    end
end

%% scan datadirs

locs = containers.Map;
locs('Seagate') = '/mnt/usbstick/proton_data';
locs('Matti') = '/mnt/matti/data';
locs('Vertical') = '/mnt/vertical/proton_data';

dirs = containers.Map;

for l=unique(surfaces.DataLocation1)'
    try
        dir = locs(l{1});
    catch
        continue;
    end

    thisdir = containers.Map;
    for i=find(strcmp(surfaces.DataLocation1, l{1}))'
        if ~isempty(surfaces.StickDate{i})
            k = sprintf('%s_%s', surfaces.StickDate{i}, 'stickcam');
            thisdir(k) = true;
        end
        if ~isempty(surfaces.OptoDate{i})
            k = sprintf('%s_%s', surfaces.OptoDate{i}, 'optocam');
            thisdir(k) = true;
        end
        if ~isempty(surfaces.BioDate{i})
            k = sprintf('%s_%s', surfaces.BioDate{i}, 'biocam');
            thisdir(k) = true;
        end
    end
    dirs(dir) = thisdir;
end

for l=unique(surfaces.DataLocation2)'
    try
        dir = locs(l{1});
        idx = find(strcmp(surfaces.DataLocation2, l{1}));
    catch
        try
            l = strsplit(l{1}, '/');
            dir = locs(l{1});
            idx = find(cellfun(@(s) strfind(s, l{1}), surfaces.DataLocation2));
        catch
            continue;
        end
    end

    thisdir = containers.Map;
    for i=idx'
        if ~isempty(surfaces.StickDate{i})
            k = sprintf('%s_%s', surfaces.StickDate{i}, 'stickcam');
            thisdir(k) = true;
        end
        if ~isempty(surfaces.OptoDate{i})
            k = sprintf('%s_%s', surfaces.OptoDate{i}, 'optocam');
            thisdir(k) = true;
        end
        if ~isempty(surfaces.BioDate{i})
            k = sprintf('%s_%s', surfaces.BioDate{i}, 'biocam');
            thisdir(k) = true;
        end
    end
    dirs(dir) = thisdir;
end

%% prep to calculate

cal_results;
calibs = containers.Map;
calibs('stickcam') = {'20170523', '1'};
calibs('optocam')  = {'20170523', '2'};
calibs('biocam')   = {'20170215', '3'};

all_mats = {};
all_props = [];

%% calculate learning objectives

for dir=dirs.keys
    DATADIR = dir{1};
    fprintf('%s\n', DATADIR);

    errors = containers.Map;
    for dateff=dirs(dir{1}).keys
        parts = strsplit(dateff{1}, '_');
        date = parts{1};
        eff = parts{2};

        if any(cellfun(@(s) ~isempty(strfind(s, sprintf('%s-%s', date, eff(1:length(eff)-3)))), all_mats))
            fprintf('\tskip %s\n', dateff{1});
            continue;
        else
            fprintf('\t%s\n', dateff{1});
        end

        try
            [data, materials, calib] = icra17_load(DATADIR, date, eff, @(x) false, calibs(eff));
            data = icra17_process('bluefox', data, calib, H_vic2bod, H_m402bod, H_bal2imu);
        catch err
            fprintf('ERROR: %s\n', err.message);
            disp(getReport(err));
            errors(sprintf('%s_%s', date, eff)) = err;
            continue;
        end

        for m = 1:length(materials)
            fprintf('\t\t%s\n', materials{m});

            try
                fprintf('%s%s %s\n', date, eff, materials{m});
                d = data(materials{m});

                [~, taps] = icra17_process_taps(d);
                tmask = false(size(d.tmask));
                hmask = false(size(d.tmask));
                for i=1:length(taps)
                    hmask(taps(i)-750:taps(i)-50) = 1;
                    tmask(taps(i)-50:taps(i)+750) = 1;
                end

                feats = {'SC' 'TR' 'WV' 'SP' 'F' 'RG' 'Fr' 'FM'};
                props = steinbach_features(feats, hmask, tmask, d.smask, struct('data', dft321(d.bai(:,2:4)), 'Fs', 3000), struct('data', sqrt(sum(d.biws(:,2:3).^2,2)), 'Fs', 3000), []);

                all_mats = [all_mats; sprintf('%s-%s-%s', date, eff(1:length(eff)-3), d.i)];
                all_props = [all_props; props];
            catch err
                fprintf('ERROR: %s\n', err.message);
                disp(getReport(err));
                errors(sprintf('%s_%s_%s', date, eff, d.i)) = err;
            end
        end
    end
end

%% download extracted features from berkeley

% berkeley$ ~/caffe/build/tools/extract_features minc-googlenet.caffemodel train_val-googlenet.prototxt pool5/7x7_s1,fc8-20 minc-googlenet-feats_test_7x7s1,minc-googlenet-feats_test_fc8 17 lmdb GPU 1
% # edit train_val-googlenet.prototxt to use train_images.txt in test mode
% berkeley$ ~/caffe/build/tools/extract_features minc-googlenet.caffemodel train_val-googlenet.prototxt pool5/7x7_s1,fc8-20 minc-googlenet-feats_train_7x7s1,minc-googlenet-feats_train_fc8 61 lmdb GPU 1
% $ scp -r alexburka@durian9.banatao.berkeley.edu:/x/alexburka/minc-model/minc-googlenet-feats* .
% $ scp alexburka@durian9.banatao.berkeley.edu:/x/alexburka/crops/all_crops/{train,test}_*.txt .

%s1 = lmdb.DB('checked-feats_train_7x7s1');
%s1t = lmdb.DB('checked-feats_test_7x7s1');
%s1 = lmdb.DB('tcnn3-checked_train_fc7');
%s1t = lmdb.DB('tcnn3-checked_test_fc7');
%s1val = s1.values;
%s1tval = s1t.values;
train = readtable('train_folds.txt', 'Delimiter',' ', 'ReadVariableNames',false);
test = readtable('test_images.txt', 'Delimiter',' ', 'ReadVariableNames',false);
%wo = table2array(train(:,2)) ~= 11;
%wot = table2array(test(:,2)) ~= 11;
wo = cellfun(@(r) ~strcmp(subsref(strsplit(r, '/'), substruct('{}', {2})), 'other'), train.Var1);
wot = cellfun(@(r) ~strcmp(subsref(strsplit(r, '/'), substruct('{}', {2})), 'other'), test.Var1);
load tcnn3_conv
%s1conv = double(cell2mat(cellfun(@(v) squeeze(caffe_pb.fromDatum(v)), s1val(wo), 'uniform',false)));
%s1convt = double(cell2mat(cellfun(@(v) squeeze(caffe_pb.fromDatum(v)), s1tval(wot), 'uniform',false)));
%s1mod = svmtrain(table2array(train(wo,2)), double(cell2mat(cellfun(@(v) squeeze(caffe_pb.fromDatum(v)), s1val(wo), 'uniform',false)))', '-m 1000 -s 0 -t 2');
%s1Y = svmpredict(table2array(train(wo,2)), double(cell2mat(cellfun(@(v) squeeze(caffe_pb.fromDatum(v)), s1val(wo), 'uniform',false)))', s1mod);
%s1Yt = svmpredict(table2array(test(wot,2)), double(cell2mat(cellfun(@(v) squeeze(caffe_pb.fromDatum(v)), s1tval(wot), 'uniform',false)))', s1mod);

%% big learning setup

splits = train.Var2;
folds = unique(splits);

props = {'B1' 'B10' 'MF1' 'MF13' 'H' 'SP' 'WV' 'F' 'FF' 'hH' 'hR' 'hSS' 'hW' 'hBS' 'hOS' 'MF4' 'MF7' 'MF10' 'FM' 'Fr' 'RG' 'TR' 'SC'};
mods = cell(length(props), length(folds)+1);
trains = cell(length(props), length(folds)+1);
tests = cell(length(props), length(folds)+1);
Y = cell(length(props), length(folds)+1);
Yt = cell(length(props), length(folds)+1);
tvkeys = cell(length(props), 1);
cols = cell(length(props), length(folds)+1);

nu = [0.2 0.4 0.6 0.8];
C = [1 10 100 1000];
gamma = [0.0005 0.001 0.005];
gs_idx = {prepare_grid(nu, gamma) prepare_grid(C, gamma)};
gs_acc = {zeros(length(folds), size(gs_idx{1},1)) zeros(length(folds), size(gs_idx{2},1))};
gs = zeros(1, length(props));

%% big learning

for p = PROPS_TO_DO
    tic;
    fprintf('%d %s', p, props{p});
    if strcmp(props{p}, 'FF')
        tvN = readtable('train_Fn.txt', 'Delimiter',' ', 'ReadVariableNames',false);
        tvT = readtable('train_Ft.txt', 'Delimiter',' ', 'ReadVariableNames',false);
        trainval = table(tvN.Var1, tvN.Var2 ./ tvT.Var2);
        tvN = readtable('test_Fn.txt', 'Delimiter',' ', 'ReadVariableNames',false);
        tvT = readtable('test_Ft.txt', 'Delimiter',' ', 'ReadVariableNames',false);
        holdout = table(tvN.Var1, tvN.Var2 ./ tvT.Var2);
    else
        trainval = readtable(sprintf('train_%s.txt', props{p}), 'Delimiter',' ', 'ReadVariableNames',false);
        holdout = readtable(sprintf('test_%s.txt', props{p}), 'Delimiter',' ', 'ReadVariableNames',false);
    end
    tvkeys{p} = cellfun(@(s) str2num(cell2mat(subsref(regexp(s, 'checked/[^/]*/(\d+)_[^_]+_(\d+)', 'tokens'), substruct('{}', {1})))), trainval.Var1);
    if isempty(mods{p,1})
        for gsi=1:size(gs_idx{(props{p}(1)=='h')+1},1)
            fprintf(' %d-', gsi);
            for f=1:length(folds)
                fprintf('%d', f);
                if props{p}(1) == 'h'
                    gsp = [C(gs_idx{2}(gsi,1)), gamma(gs_idx{2}(gsi,2))];
                else
                    gsp = [nu(gs_idx{1}(gsi,1)), gamma(gs_idx{1}(gsi,2))];
                end
                [trains{p,f}, tests{p,f}, cols{p,f}, mods{p,f}, Y{p,f}, Yt{p,f}] = ...
                    toh17_trainmodel(s1conv(:,splits ~= folds(f))', s1conv(:,splits == folds(f))', ...
                                     trainval(splits ~= folds(f),2), trainval(splits == folds(f),2), ...
                                     cols{p,f}, props{p}, gsp);
                if props{p}(1) == 'h'
                    gs_acc{2}(f,gsi) = nnz(tests{p,f} == Yt{p,f}')/length(Yt{p,f});
                else
                    cc = corrcoef(tests{p,f}, Yt{p,f});
                    gs_acc{1}(f,gsi) = cc(2,1);
                end
            end
        end
        [~,gs(p)] = max(mean(gs_acc{(props{p}(1)=='h')+1}, 1));
        gsi = gs(p);
        fprintf(' gsi=%d', gs(p));
        toh17_machine_learning_for_gsi_1_size_gs_id_47;
    end
    if isempty(mods{p,end})
        fprintf(' holdout');
        if props{p}(1) == 'h'
            gsp = [C(gs_idx{2}(gs(p),1)), gamma(gs_idx{2}(gs(p),2))];
        else
            gsp = [nu(gs_idx{1}(gs(p),1)), gamma(gs_idx{1}(gs(p),2))];
        end
        [trains{p,end}, tests{p,end}, cols{p,end}, mods{p,end}, Y{p,end}, Yt{p,end}] = ...
            toh17_trainmodel(s1conv', s1convt', ...
                             trainval(:,2), holdout(:,2), ...
                             cols{p,end}, props{p}, gsp);
    end
    fprintf(' (%g s)\n', toc);
end
%% eval
corrs = zeros(length(props),3);
slopes = zeros(length(props),3);
pvals = zeros(length(props),3);
fvus = zeros(length(props),3);
precs = zeros(length(props),3);
recs = zeros(length(props),3);
errs = zeros(length(props),3);
outputs = cell(length(props),3);
for p=1:length(props)
    if ~isempty(Y{p,1})
        if props{p}(1) == 'h'
            for f=1:length(folds)
                [corr, pval, prec, rec, err] = toh17_evalmodel(props{p}, trains{p,f}, Y{p,f});
                corrs(p,1) = corrs(p,1) + corr/length(folds);
                pvals(p,1) = pvals(p,1) + pval/length(folds);
                precs(p,1) = precs(p,1) + prec/length(folds);
                recs(p,1) = recs(p,1) + rec/length(folds);
                errs(p,1) = errs(p,1) + err/length(folds);
                [corr, pval, prec, rec, err] = toh17_evalmodel(props{p}, tests{p,f}, Yt{p,f});
                corrs(p,2) = corrs(p,2) + corr/length(folds);
                pvals(p,2) = pvals(p,2) + pval/length(folds);
                precs(p,2) = precs(p,2) + prec/length(folds);
                recs(p,2) = recs(p,2) + rec/length(folds);
                errs(p,2) = errs(p,2) + err/length(folds);
            end
            [corrs(p,3), pvals(p,3), precs(p,3), recs(p,3), errs(p,3)] = toh17_evalmodel(props{p}, trains{p,end}, Y{p,end});
            [corrs(p,4), pvals(p,4), precs(p,4), recs(p,4), errs(p,4)] = toh17_evalmodel(props{p}, tests{p,end}, Yt{p,end});
            for o=1:4
                outputs{p,o} = sprintf('%.3f (p=%.2g, prec=%.2g, rec=%.2g, diff=%.2g)', corrs(p,o), pvals(p,o), precs(p,o), recs(p,o), errs(p,o));
            end
        else
            for f=1:length(folds)
                [corr, slope, pval, fvu, err] = toh17_evalmodel(props{p}, trains{p,f}, Y{p,f});
                corrs(p,1) = corrs(p,1) + corr/length(folds);
                slopes(p,1) = slopes(p,1) + slope/length(folds);
                pvals(p,1) = pvals(p,1) + pval/length(folds);
                fvus(p,1) = fvus(p,1) + fvu/length(folds);
                errs(p,1) = errs(p,1) + err/length(folds);
                [corr, slope, pval, fvu, err] = toh17_evalmodel(props{p}, tests{p,f}, Yt{p,f});
                corrs(p,2) = corrs(p,2) + corr/length(folds);
                slopes(p,2) = slopes(p,2) + slope/length(folds);
                pvals(p,2) = pvals(p,2) + pval/length(folds);
                fvus(p,2) = fvus(p,2) + fvu/length(folds);
                errs(p,2) = errs(p,2) + err/length(folds);
            end
            [corrs(p,3), slopes(p,3), pvals(p,3), fvus(p,3), errs(p,3)] = toh17_evalmodel(props{p}, trains{p,end}, Y{p,end});
            [corrs(p,4), slopes(p,4), pvals(p,4), fvus(p,4), errs(p,4)] = toh17_evalmodel(props{p}, tests{p,end}, Yt{p,end});
            for o=1:4
                outputs{p,o} = sprintf('%.3f (m=%.2g, p=%.2g, fvu=%.2g, rmse=%.2g)', corrs(p,o), slopes(p,o), pvals(p,o), fvus(p,o), errs(p,o));
            end
        end
    end
end

prop_names = {
    'Frequency bin 1'
    'Frequency bin 10'
    'MF cepstral coeff. 1'
    'MF cepstral coeff. 13'
    'Hardness'
    'Spikiness'
    'Waviness'
    'Fineness'
    'Friction coefficient'
    'Hardness'
    'Roughness'
    'Stickiness (tooling ball)'
    'Warmness'
    'Stickiness (BioTac)'
    'Stickiness (OptoForce)'
    'MF cepstral coeff. 4'
    'MF cepstral coeff. 7'
    'MF cepstral coeff. 10'
    'Friction (normalized)'
    'Friction'
    'Regularity'
    'Temporal roughness'
    'Spectral centroid'
};
prop_order = [1 2 9 5 17 3 10 11 12 4 6 16 7 15 8 14 13];
rate_order = [1 2 4 3 6 5];

for p=1:length(props)
    fprintf('plotting %s\n', props{p});
    clf;
    toh17_plotmodel(cell2mat(trains(p,1:length(folds))), cell2mat(Y(p,1:length(folds))')', props{p}, 'cvtrain');
    toh17_plotmodel(cell2mat(tests(p,1:length(folds))), cell2mat(Yt(p,1:length(folds))')', props{p}, 'cvtest');
    toh17_plotmodel(trains{p,end}, Y{p,end}', props{p}, 'alltrain');
    toh17_plotmodel(tests{p,end}, Yt{p,end}', props{p}, 'alltest');
end

h = arrayfun(@(p) props{p}(1) == 'h', 1:length(props));
tabcfg = struct;
tabcfg.tablePlacement = 'tb';
tabcfg.tableBorders = false;
tabcfg.dataFormat = {'%.2f'};

tabcfg.tableRowLabels = prop_names(~h);
tabcfg.data = num2cell([corrs(~h,1) slopes(~h,1) pvals(~h,1) 1-fvus(~h,1) errs(~h,1) corrs(~h,2) slopes(~h,2) pvals(~h,2) 1-fvus(~h,2) errs(~h,2)]);
fprintf('\n\nPROPERTIES CV\n');
toh17_maketable(tabcfg, prop_order, [3 8]);
tabcfg.data = num2cell([corrs(~h,3) slopes(~h,3) pvals(~h,3) 1-fvus(~h,3) errs(~h,3) corrs(~h,4) slopes(~h,4) pvals(~h,4) 1-fvus(~h,4) errs(~h,4)]);
fprintf('\n\nPROPERTIES ALL\n');
toh17_maketable(tabcfg, prop_order, [3 8]);

tabcfg.tableRowLabels = prop_names(h);
tabcfg.data = num2cell([100*corrs(h,1) pvals(h,1) precs(h,1) recs(h,1) errs(h,1) 100*corrs(h,2) pvals(h,2) precs(h,2) recs(h,2) errs(h,2)]);
fprintf('\n\nRATINGS CV\n');
toh17_maketable(tabcfg, rate_order, [2 7]);
tabcfg.data = num2cell([100*corrs(h,3) pvals(h,3) precs(h,3) recs(h,3) errs(h,3) 100*corrs(h,4) pvals(h,4) precs(h,4) recs(h,4) errs(h,4)]);
fprintf('\n\nRATINGS ALL\n');
toh17_maketable(tabcfg, rate_order, [2 7]);

for p=1:length(props)
    fprintf('\\csdef{propname %s}{%s}\n', props{p}, prop_names{p});
end

table(props', outputs(:,1), outputs(:,2), 'RowNames',arrayfun(@num2str, 1:length(props), 'uniform',0), 'VariableNames',{'Property' 'TrainCV' 'TestCV'})
table(props', outputs(:,3), outputs(:,4), 'RowNames',arrayfun(@num2str, 1:length(props), 'uniform',0), 'VariableNames',{'Property' 'Train' 'Hold'})

