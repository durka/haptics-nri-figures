%% setup
addpath(genpath('RANSAC-Toolbox'));
addpath(genpath('matGeom'));
DATADIRS = {
    '/mnt/usbstick/proton_data'
    '/mnt/vertical/proton_data'
    '~/ssd/data'
};
cal_results;

%% list datasets

calibs = containers.Map;
%calibs('stickcam') = {'20170220', '2'};
%calibs('optocam')  = {'20170215', '2'};
calibs('stickcam') = {'20170523', '1'};
calibs('optocam')  = {'20170523', '2'};
calibs('biocam')   = {'20170215', '3'};

dates = [221 222 223 225 227 301 303 304 306 307 308 309 312 321 323 403 404 405 406 407 413 418 419 420 421 426 427 429 501 502 503 504 508 509 516 518 519 523 613 614 615 620 621 626 627 630 705 724 727 811 828 829 831 906 907 921 922 1004 1005 1008];
%dates = [613 614 615 621 626 627 630 705 724 727 811 906 907 915 917 921 922 1004 1005 1008];
specs = {};
for i = 1:length(dates)
    specs = [ specs;
              {
                  { sprintf('2017%04d', dates(i)), 'stickcam' };
                  { sprintf('2017%04d', dates(i)), 'optocam'  };
                  { sprintf('2017%04d', dates(i)), 'biocam'   };
              }
            ];
end

%% load and process

allprops = {zeros(0,3) zeros(0,2)
            zeros(0,3) zeros(0,2)
            zeros(0,3) zeros(0,2)};
allsrcs = {zeros(0,3) zeros(0,3)
           zeros(0,3) zeros(0,3)
           zeros(0,3) zeros(0,3)};
errors = {containers.Map containers.Map};

for q = 1:length(specs)
    date = specs{q}{1};
    flow = specs{q}{2};
    fprintf('PROCESSING %s/%s\n', date, flow);

    DATADIR = 'nope';
    for di=1:length(DATADIRS)
        if exist(fullfile(DATADIRS{di}, date, flow), 'file')
            DATADIR = DATADIRS{di};
            break;
        end
    end
    if strcmp(DATADIR, 'nope')
        continue;
    end

    try
        filename = fullfile(DATADIR, date, flow, 'props.mat');
        if exist(filename, 'file') > 0 && ~isempty(whos(matfile(filename), 'calib'))
            load(filename);
        else
            [data, materials, calib] = icra17_load(DATADIR, date, flow, @(x) false, calibs(flow));
            data = icra17_process('bluefox', data, calib, H_vic2bod, H_m402bod, H_bal2imu);
            save(filename, 'data', 'materials', 'calib');
        end

        for m=1:length(materials)
            fprintf('\t%s...\n', materials{m});
            d = data(materials{m});
            sprops = [];

            % dragging properties
            comp = bwconncomp(d.smask);
            for c=1:length(comp.PixelIdxList)
                pre = romano_features('pre', d.biws, d.bvei, d.bai, calib.mass, 0.25*3000, 0, comp.PixelIdxList{c}([1 end]));
                rom = romano_features('post', pre, 10, 'naive', 0.1, 0);
                sprops = [sprops; rom(:,end-2:end)];
            end

            % tapping properties
            tprops = tapspeed(d, false, false);

            fj = find(strcmp({'stickcam' 'optocam' 'biocam'}, flow));
            allprops{fj,1} = [allprops{fj,1}; sprops];
            allsrcs{fj,1} = [allsrcs{fj,1}; repmat([str2num(date) str2num(d.i)], [size(sprops,1) 1])];
            allprops{fj,2} = [allprops{fj,2}; tprops(:,1:2)];
            allsrcs{fj,2} = [allsrcs{fj,2}; repmat([str2num(date) str2num(d.i)], [size(tprops,1) 1])];
        end
        clear data materials;
    catch err
        fprintf('error during outer loop: %s\n', err.message);
        disp(getReport(err));
        errors{1}(sprintf('%s_%s', date, flow)) = err;
        %keyboard
        % continue loop
    end
end

