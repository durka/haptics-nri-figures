%% load data

[amz_data, amz_attn, amz_badimg] = amazon_summarize('../nri/crops', [20171106 20171109 20171117], 'none');
datadirs = amz_data.datadir;
sources = rowfun(@(s) strsplit(s{1}, '-'), amz_data, 'InputVariables','source', 'OutputVariableNames','source');
sources = sources.source;
[~,idx] = unique(cellfun(@(dir, date, endeff) fullfile(dir, date, [endeff 'cam']), datadirs, sources(:,1), sources(:,2), 'UniformOutput',false));
dir_srcs = [datadirs(idx) sources(idx,1:2)];

surfaces = readtable('surfaces.csv', 'Format', '%q%q%q%q%q%q%q%q%q%q%q%q%q%q%q%*[^\n]');

%% figure out what to plot

cal_results;
calibs = containers.Map;
calibs('stickcam') = {'20170523', '1'};
calibs('optocam')  = {'20170523', '2'};
calibs('biocam')   = {'20170215', '3'};

all_answers = containers.Map;

for s=1:length(dir_srcs)
    fprintf('Processing %d/%d (%s-%s)...\n', s, length(dir_srcs), dir_srcs{s,2}, dir_srcs{s,3});

    flow = [dir_srcs{s,3} 'cam'];
    propfile = sprintf('%s/%s/%s_props.mat', dir_srcs{s,1}, dir_srcs{s,2}, flow);
    if false && exist(propfile, 'file')
        fprintf('Loading preprocessed data\n');
        load(propfile);
        materials = data.keys;

        %data = icra17_process('bluefox', data);

        % load answers if not already present
        if ~isfield(data(materials{1}), 'answers')
            for m=1:length(materials)
                d = data(materials{m});
                flow = parse_flow(fullfile(d.datadir, d.date, d.flow, d.i, [d.flow '.flow']));
                d.answers = flow.answers;
                all_answers(sprintf('%s-%s-%s', d.date, d.flow(1:length(d.flow)-3), d.i)) = flow.answers;
                data(materials{m}) = d;
            end
        end

        %save(propfile, 'unproc_data', 'data', 'materials', 'calib');
    else
        [data, materials, calib] = icra17_load(dir_srcs{s,1}, dir_srcs{s,2}, flow, @(x) false, calibs(flow));
        unproc_data = deepcopy(data);
        data = icra17_process('bluefox', data, calib, H_vic2bod, H_m402bod, H_bal2imu);
        save(propfile, 'unproc_data', 'data', 'materials', 'calib');
    end

    if false
        for m=1:length(materials)
            fprintf('Plotting %d/%d (%s)...\n', m, length(materials), materials{m});

            clf;
            d = data(materials{m});

            time = d.bvei(:,1)-d.bvei(1,1);
            taps = bwconncomp(d.tmask);
            taps = cell2mat(cellfun(@(l) l([1 end])', taps.PixelIdxList, 'UniformOutput',false)');

            subplot(311);
            hl_plot(time, d.bvei(:,2:4), taps);
            ylabel('Position');
            title(sprintf('%s (%s-%s-%s)', materials{m}, dir_srcs{s,2}, dir_srcs{s,3}, d.i));

            subplot(312);
            hl_plot(time, d.biws(:,2:4), taps);
            ylabel('Force');

            subplot(313);
            hl_plot(time, d.bai(:,2:4), taps);
            ylabel('Vibration');
            xlabel('Time (s)');

            print('-dpng', sprintf('%s-%s-%s.png', dir_srcs{s,2}, dir_srcs{s,3}, d.i));
        end
    end
end

%% calculate the stupid features

all_mats = {};
all_props = [];
bio_temps = containers.Map;

for s=1:length(dir_srcs)
    tmr_amz = tic;
    fprintf('Processing %d/%d (%s-%s)... ', s, length(dir_srcs), dir_srcs{s,2}, dir_srcs{s,3});

    flow = [dir_srcs{s,3} 'cam'];
    propfile = sprintf('%s/%s/%s_props.mat', dir_srcs{s,1}, dir_srcs{s,2}, flow);
    load(propfile);

    for m=1:length(materials)
        d = data(materials{m});

        % separate taps from hand acceleration
        [~, taps] = icra17_process_taps(d);
        tmask = false(size(d.tmask));
        hmask = false(size(d.tmask));
        for i=1:length(taps)
            hmask(taps(i)-750:taps(i)-50) = 1;
            tmask(taps(i)-50:taps(i)+750) = 1;
        end

        d.props = containers.Map;
        feats = {'SC' 'TR' 'WV' 'SP' 'F' 'RG' 'Fr' 'FM'};
        d.props('steinbach') = steinbach_features(feats, hmask, tmask, d.smask, struct('data', dft321(d.bai(:,2:4)), 'Fs', 3000), struct('data', sqrt(sum(d.biws(:,2:3).^2,2)), 'Fs', 3000), []);

        all_mats = [all_mats; sprintf('%s-%s-%s', dir_srcs{s,2}, dir_srcs{s,3}, d.i)];
        if isempty(d.sensor.bio)
            all_props = [all_props; d.props('steinbach') NaN NaN];
        else
            d.props('warm') = mean(d.sensor.bio(:,[25 26]));
            all_props = [all_props; d.props('steinbach') d.props('warm')];
            bio_temps([d.date '-' d.flow(1:length(d.flow)-3) '-' d.i]) = d.sensor.bio;
        end

        data(materials{m}) = d;
    end
    
    save(propfile, 'unproc_data', 'data', 'materials', 'calib');
    fprintf('%.1fs\n', toc(tmr_amz));
end

%% correlate

study_props = [];
n = {'hard' 'rough' 'sticky' 'warm'};
nn = [cellfun(@(s) ['exp_' s], n, 'UniformOutput',false) cellfun(@(s) ['turk_' s], n, 'UniformOutput',false)];

for m=1:length(all_mats)
    rows = strcmp(amz_data.source, all_mats{m});
    study_props = [study_props;
                   nanmean(table2array(amz_data(rows, nn)))];
end

endeffs = {'stick' 'opto' 'bio'};
nn = [cellfun(@(s) ['E' s(1)], n, 'UniformOutput',false) cellfun(@(s) ['T' s(1)], n, 'UniformOutput',false)];
clf;
for e=1:3
    rows = not(cellfun(@isempty, strfind(all_mats, endeffs{e})));

    % copy experimenter roughness and stickiness from bio
    if ~strcmp(endeffs{e}, 'bio')
        for m=1:length(all_mats)
            if rows(m)
                parts = strsplit(all_mats{m}, '-');
                i = find(strcmp(table2cell(surfaces(:,[upper(endeffs{e}(1)) endeffs{e}(2:end) 'Date'])), parts{1}) & strcmp(table2cell(surfaces(:,[upper(endeffs{e}(1)) endeffs{e}(2:end) 'Num'])), parts{3}), 1);
                sidx = [1 2 4];
                aidx = 9;
                if isempty(i)
                    fprintf('no such %s :(\n', all_mats{m});
                elseif isempty(surfaces.BioDate{i})
                    fprintf('no bio for %s :(\n', all_mats{m});
                else
                    bio = [surfaces.BioDate{i} '-bio-' surfaces.BioNum{i}];
                    mbio = find(strcmp(all_mats, bio), 1);
                    if isempty(mbio)
                        fprintf('no such %s :(\n', bio);
                    else
                        study_props(m,sidx) = study_props(mbio,sidx);
                        all_props(m,aidx) = all_props(mbio,aidx);
                    end
                end
            end
        end
    end
    
    amazon_summarize_conf(130+e, corr(all_props(rows,:), study_props(rows,:), 'rows','pairwise'), endeffs{e}, nn, [feats 'T'], 4);
end

