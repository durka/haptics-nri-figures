clf;
ps={'SC' 'WV' 'TR' 'SP' 'RG' 'F' 'Fr' 'FM'};
modes = {'train' 'test'};
for p=1:length(ps)
    disp(ps{p});
    for m=1:length(modes)
        subplot(length(ps),2,(p-1)*2+m);

        data = readtable(sprintf('~/Downloads/%s_%s.txt', modes{m}, ps{p}), ...
                         'Delimiter',' ' ,'ReadVariableNames',false);
        x = data.Var2;
        
        % use train stats
        if m == 1
            av = mean(x);
            st = nanstd(filloutliers(x, 'previous'));
        end
        x = (x - av)/st;

        plot(x);
        maxis([3 4], [-2 2]);
        axis tight;
        title(sprintf('%s (%s)', ps{p}, modes{m}));

        data.Var2 = x;
        writetable(data, sprintf('%s_%s.txt', modes{m}, ps{p}), ...
                   'Delimiter',' ', 'WriteVariableNames',false);
    end
end
