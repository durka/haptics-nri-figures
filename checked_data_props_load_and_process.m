effs = {'stickcam' 'optocam' 'biocam'};
dates = unique(specs(:,1));
errors = containers.Map;
for dat=1:length(dates)
    date = num2str(dates(dat));

    %for eff=1:length(effs)
        flow = effs{eff};
        all_nums = sort(specs(specs(:,1)==str2num(date) & specs(:,2)==eff, 3));
        
        have_img = arrayfun(@(n) exist(fullfile(OUTDIR, sprintf('%s_%s_%d.png', date, flow, n)), 'file'), all_nums) ~= 0;
        have_prop = arrayfun(@(n) checked_props.isKey(sprintf('%s_%s_%d', date, flow, n)) && ~all(isnan(subsref(checked_props, substruct('()', {sprintf('%s_%s_%d', date, flow, n)}, '()', {'steinbach'})))), all_nums);
        all_nums = all_nums(~have_img | ~have_prop);
        
        system(sprintf('mkdir -p ../../nri/data/%s/%s', date, flow));
        batches = 1:5:length(all_nums);
        for b=1:length(batches)
            nums = all_nums(batches(b):(min(batches(b)+4,end)));
            if length(unique(nums)) < length(nums)
                error('duplicate episode');
            end
            if ~isempty(nums)
                tnow = clock;
                fprintf('\n[%02d:%02d] PROCESSING %d/%d %s %s %s\n\n', tnow(4), tnow(5), dat, length(dates), date, flow, mat2str(nums'));

                DATADIR = 'nope';
                for di=1:length(DATADIRS)
                    if exist(fullfile(DATADIRS{di}, date, flow), 'file')
                        DATADIR = DATADIRS{di};
                        break;
                    end
                end
                if strcmp(DATADIR, 'nope')
                    fprintf('\tmissing data\n');
                    continue;
                end

                clear data materials calib;
                clf;

                matname = fullfile(OUTDIR, sprintf('%s_%s_%s.mat', date, flow, strjoin(arrayfun(@num2str, nums, 'uniform',0), '-')));
                if exist(matname, 'file')
                    load(matname);
                else
                    for n=1:length(nums)
                        system(sprintf('rsync -avz ''--exclude=*.png'' ''--exclude=*.dat'' %s/%s/%s/%d ../../nri/data/%s/%s', DATADIR, date, flow, nums(n), date, flow));
                    end
                    [data, materials, calib] = icra17_load('../../nri/data', date, flow, @(x) x~=nums, calibs(flow));
                    data = icra17_process('bluefox', data, calib, H_vic2bod, H_m402bod, H_bal2imu);
                    save(matname, 'data', 'materials', 'calib');
                end

                if eff == 1
                    N = 4;
                else
                    N = 5;
                end
                for m=1:length(materials)
                    d = data(materials{m});
                    
                    if ~checked_props.isKey(sprintf('%s_%s_%s', date, flow, d.i)) || all(isnan(subsref(checked_props, substruct('()', {sprintf('%s_%s_%s', date, flow, d.i)}, '()', {'steinbach'}))))
                        props = containers.Map;

                        flowrec = parse_flow(fullfile(DATADIR, date, flow, d.i, [flow '.flow']));
                        props('ratings') = flowrec.answers;

                        [~, taps] = icra17_process_taps(d);
                        tmask = false(size(d.tmask));
                        hmask = false(size(d.tmask));
                        for i=1:length(taps)
                            hmask(taps(i)-750:taps(i)-50) = 1;
                            tmask(taps(i)-50:taps(i)+750) = 1;
                        end

                        feats = {'MF' 'H' 'SC' 'TR' 'WV' 'SP' 'F' 'RG' 'Fr' 'FM'};
                        rom = [];
                        stein = [];
                        comp = bwconncomp(d.smask);
                        try
                            for c=1:length(comp.PixelIdxList)
                                if nnz(comp.PixelIdxList{c}) >= 1500
                                    stein = [stein; steinbach_features(feats, hmask, tmask, comp.PixelIdxList{c}([1 end]), struct('data', dft321(d.bai(:,2:4)), 'Fs', 3000), struct('data', sqrt(sum(d.biws(:,2:3).^2,2)), 'Fs', 3000), [])];
                                end

                                pre = romano_features('pre', d.biws, d.bvei, d.bai, calib.mass, 0.25*3000, 0, comp.PixelIdxList{c}([1 end]));
                                rom = [rom; romano_features('post', pre, 10, 'naive', 0.1, 0)];
                            end
                        catch err
                            errors(sprintf('%s_%s_%s', date, flow, d.i)) = err;
                        end
                        props('romano') = nanmean(rom, 1);
                        props('steinbach') = nanmean(stein, 1);

                        checked_props(sprintf('%s_%s_%s', date, flow, d.i)) = props;
                    end

                    if exist(fullfile(OUTDIR, sprintf('%s_%s_%d.png', date, flow, m)), 'file') == 0
                        clf;
                        subplot(N,1,1);
                        plot(d.bvei(:,1)-d.bvei(1,1), d.bvei(:,2:4));
                        ylabel('Position');
                        subplot(N,1,2);
                        plot(d.biws(:,1)-d.biws(1,1), d.biws(:,2:4));
                        ylabel('Force');
                        subplot(N,1,3);
                        plot(d.bai(:,1)-d.bai(1,1), d.bai(:,2:4));
                        ylabel('Vibration');
                        subplot(N,1,4);
                        plot(d.imu.mic(:,1)-d.imu.mic(1,1), d.imu.mic(:,2));
                        ylabel('Sound');
                        if eff == 2
                            subplot(N,1,5);
                            plot(d.sensor.opto(:,1)-d.sensor.opto(1,1), d.sensor.opto(:,2:4));
                            ylabel('Opto');
                        elseif eff == 3
                            subplot(N,1,5);
                            plot(d.sensor.bio(:,1)-d.sensor.bio(1,1), d.sensor.bio(:,2:end));
                            ylabel('Bio');
                        end
                        suplabel(sprintf('%s (%s)', materials{m}, flowrec.answers('surface name').text), 't');
                        print('-dpng', '-r200', fullfile(OUTDIR, sprintf('%s_%s_%s.png', date, flow, d.i)));
                    end
                end
            end
        end

        system(sprintf('rm -rf ../../nri/data/%s/%s', date, flow));
    %end
end
