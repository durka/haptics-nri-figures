% MAXIS modifies one or more axis limits
%
% Modifies the axis limits in-place as well as
% returning the value.
%
% Parameters:
%   - idx: index or indices of values to modify
%   - val: value or values to insert
% Returns:
%   - new axis limits
%
% Examples:
%    maxis(1, 0); % make X axis start at zero
%    maxis([3 4], [50 100]); % set Y axis limits to 50-100
function ax = maxis(idx, val)
    ax = axis;
    ax(idx) = val;
    axis(ax);
end

