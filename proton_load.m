function [data, idx] = proton_load(data_dir, surface_names, endeff)
    addpath(genpath('RANSAC-Toolbox'))
    addpath('libsvm/matlab')

    if ischar(surface_names)
        surface_names = {surface_names};
    end

    flowtypes = containers.Map({'tooling_ball', 'optoforce', 'biotac'}, {'stickcam', 'optocam', 'biocam'});
    try
        flowtype = flowtypes(lower(endeff));
    catch
        throw(MException('Proton:UnknownEndEffector', sprintf('Unknown end effector %s (please pass one of %s)\n', endeff, strjoin(flowtypes.keys, ', '))));
    end

    weights = containers.Map({'stickcam', 'optocam', 'biocam'}, {{'20170523' '1'}, {'20170523' '2'}, {'20170215' '3'}});

    switch flowtype
    case 'stickcam'
        data = struct('name', [], 'pose', [], 'force', [], 'accel', []);
    case 'optocam'
        data = struct('name', [], 'pose', [], 'force', [], 'accel', [], 'tip_force', []);
    case 'biocam'
        data = struct('name', [], 'pose', [], 'force', [], 'accel', [], 'biotac', []);
    end
    found = containers.Map;

    fprintf('Looking for %s scans of %d surface(s) under %s...\n', endeff, length(surface_names), data_dir);
    episodes = dir([data_dir '/*/' flowtype]);
    for e=1:length(episodes)
        if episodes(e).isdir && episodes(e).name(1) ~= '.'
            flow = parse_flow([episodes(e).folder '/' episodes(e).name '/' flowtype '.flow']);
            material = flow.answers('surface name').text;
            for s=1:length(surface_names)
                if strcmp(lower(material), lower(surface_names{s}))
                    fprintf('\t%s: %s/%s\n', surface_names{s}, episodes(e).folder, episodes(e).name);
                    parts = strsplit(episodes(e).folder, '/');
                    if ~isKey(found, parts{end-1})
                        found(parts{end-1}) = [];
                    end
                    found(parts{end-1}) = [found(parts{end-1}) str2num(episodes(e).name)];
                end
            end
        end
    end

    raw_data = {};
    cal_results;

    fprintf('\n\n==================== Loading data...    ===================\n\n');
    dates = keys(found);
    for f=1:length(dates)
        fprintf('Loading episode(s) %s from %s...\n', strjoin(arrayfun(@num2str, found(dates{f}), 'UniformOutput',false), ', '), dates{f});
        [d, m, c] = icra17_load(data_dir, dates{f}, flowtype, @(x) ~any(x == found(dates{f})), weights(flowtype));
        raw_data{f} = {d m c};
        fprintf('\n\n');
    end

    fprintf('\n\n==================== Processing data... ===================\n\n');
    cooked_data = {};
    di = 1;
    for f=1:length(found)
        m = raw_data{f}{2};
        fprintf('Processing %s (%d episode(s))...\n', dates{f}, length(found(dates{f})));
        cooked_data{f} = icra17_process('bluefox', raw_data{f}{1}, raw_data{f}{3}, H_vic2bod, H_m402bod, H_bal2imu);
        fprintf('\n\n');

        for i=1:length(m)
            data(di).name = m{i};
            data(di).pose = cooked_data{f}(m{i}).bvei;
            data(di).force = cooked_data{f}(m{i}).biws;
            data(di).accel = cooked_data{f}(m{i}).bai;
            switch flowtype
            case 'optocam'
                data(di).tip_force = cooked_data{f}(m{i}).sensor.opto;
            case 'biocam'
                data(di).biotac = cooked_data{f}(m{i}).sensor.bio;
            end
            di = di+1;
        end
    end
end

