function stddev = reprocess(data, xf, H_bal2imu)
    fprintf('evaluating %s... ', mat2str(xf, 6));
    xf = struct('a', xf(1), 'tx', xf(2), 'tz', xf(3));

    cal_results;
    
    save tmp.mat data;
    tmp = load('tmp.mat');
    data = tmp.data;
    clear tmp;
    !rm tmp.mat

    materials = data.keys;
    for m = 1:length(materials)
        d = data(materials{m});
        
        apr = d.april;
        nums = sort(cell2mat(apr.keys));
        pos = zeros(length(nums),3);
        motrak = d.v;
        for i=1:length(nums)
            ts = d.v(i,1);
            dat(i).t = ts;
            dat(i).id = apr(nums(i)).ids;
            dat(i).p0 = apr(nums(i)).centers;
            dat(i).p1 = apr(nums(i)).p1s;
            dat(i).p2 = apr(nums(i)).p2s;
            dat(i).p3 = apr(nums(i)).p3s';
            dat(i).p4 = apr(nums(i)).p4s;
            [X,Q] = estimate_pose_pnp(dat(i), xf);
            H_matrix = [quat2rotm(Q'),X(1:3);[0,0,0,1]];
            newMatrix = H_matrix*[1 0 0 -254.3402+191.08152942; 0 -1 0 0; 0 0 -1 30.96486407; 0 0 0 1]*H_bal2imu;
            pos(i,:) = newMatrix(1:3,4);
            motrak(i,2:4) = pos(i,:);
            motrak(i,5:7) = xfconv(newMatrix(1:3,1:3));
        end
        
        d.v = motrak;
        d.motrak = motrak;
        data(materials{m}) = d;
    end
    
    stddev = std(d.v(500:850,4));
    fprintf(' %g\n', stddev);
end
