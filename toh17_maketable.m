function toh17_maketable(tabcfg, order, pcol)
    tabcfg.tableRowLabels = tabcfg.tableRowLabels(order);
    tabcfg.data(:,pcol) = cellfun(@(p) subsref({p, '$<10^{-100}$'}, substruct('{}', {(p<1e-100)+1})), tabcfg.data(:,pcol), 'uniform',0);
    tabcfg.data = cellfun(@(s) regexprep(num2str(s, '%.3g'), '([^e]+)e-0?(.*)', '\$$1*10^{-$2}\$'), tabcfg.data, 'uniform',0);
    tabcfg.data = tabcfg.data(order,:);
    latexTable(tabcfg);
end

