% TODO find out why this produces different results from minc_model/test.py

function crops_data = prepare_image(fname, mean_data, crop_dim)
% ------------------------------------------------------------------------
im = imread(fname);
image_dim = crop_dim;

% Convert an image returned by Matlab's imread to im_data in caffe's data
% format: W x H x C with BGR channels
im_data = im(:, :, [3, 2, 1]);  % permute channels from RGB to BGR
im_data = permute(im_data, [2, 1, 3]);  % flip width and height
im_data = single(im_data);  % convert from uint8 to single
im_data = imresize(im_data, [image_dim image_dim], 'bilinear');  % resize im_data
im_data = bsxfun(@minus, im_data, reshape(mean_data, [1 1 3]));  % subtract mean_data

% oversample (4 corners, center, and their x-axis flips)
crops_data = zeros(crop_dim, crop_dim, 3, 10, 'single');
indices = [0 image_dim-crop_dim] + 1;
n = 1;
for i = indices
  for j = indices
    crops_data(:, :, :, n) = im_data(i:i+crop_dim-1, j:j+crop_dim-1, :);
    crops_data(:, :, :, n+5) = crops_data(end:-1:1, :, :, n);
    n = n + 1;
  end
end
center = floor(image_dim / 2) + 1;
half_crop = floor(crop_dim / 2);
crops_data(:,:,:,5) = ...
  im_data(center-half_crop:center+half_crop-1,center-half_crop:center+half_crop-1,:);
crops_data(:,:,:,10) = crops_data(end:-1:1, :, :, 5);

