function signal = icra17_process_smooth(signal, idx, spikes)

    for c=spikes'
        a = c-15;
        b = c+15;
        if a < 1
            signal(1:b,idx) = repmat(signal(b,idx), [b 1]);
        elseif b > size(signal,1)
            signal(a:end,idx) = repmat(signal(a,idx), [length(a:size(signal,1)) 1]);
        else
            signal(a:b,idx) = interp1([a b], signal([a b],idx), a:b);
        end
    end
    
end