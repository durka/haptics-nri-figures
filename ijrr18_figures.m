%% generates figures for the IJRR 2017 dataset paper / thesis chapter

addpath(genpath('RANSAC-Toolbox'))
addpath('libsvm/matlab')

DATADIR = '../../nri/data';

%% 3D view of dataset

% load
cal_results;
[data, materials, calib] = icra17_load('../../nri/data', '20180122', 'stickcam', @(x) false, {'20170523','1'});
data = icra17_process('bluefox', data, calib, H_vic2bod, H_m402bod, H_bal2imu);
save ../../nri/data/20180122/stickcam/props data materials calib
%%
d = data('rubber plate');

% fudge out spikes and zero point
fix = @(a,b) evalin('caller', sprintf('d.bvei(%d:%d,2:4) = interp1([%d %d], d.bvei([%d %d],2:4), %d:%d);', a,b, a,b, a,b, a,b));
fix(21807, 22281);
fix(42967, 43429);
fix(121930, 123697);
fix(122337, 123345);
d.bvei(:,4) = d.bvei(:,4) - nanmedian(d.bvei(:,4));

% plot!
f3d = figure;
plot3d(d.bvei(:,2:4), {'X position (mm)', 'Y position (mm)', 'Z position (mm)'}, {'X (mm)', 'Y (mm)', 'Z (mm)'});
quiet('colormap cool', 'MATLAB:handle_graphics:Patch:NumColorsNotEqualNumVertsException');

fsen = figure;
subplot(311);
plot(d.bvei(:,1)-d.bvei(1,1), d.bvei(:,2:4));
xlabel('Time (s)');
ylabel('Position (mm)');
legend X Y Z
subplot(312);
plot(d.biws(:,1)-d.biws(1,1), d.biws(:,2:4));
xlabel('Time (s)');
ylabel('Force (N)');
legend X Y Z
subplot(313);
plot(d.bai(:,1)-d.bai(1,1), d.bai(:,2:4));
xlabel('Time (s)');
ylabel('Acceleration (m/s^2)');
legend X Y Z

% save
figure(f3d);
print_subplots('example_eff_track_%d.pdf', 16);
figure(fsen);
print_subplots('example_eff_data_%d.pdf');

%% histograms

load allprops
pos = @(x,thresh) x(x>thresh);

figure;
lims = zeros(3,3,4);
for i=1:3
    subplot(3,3,i)
    a = find(allsrcs{i,1}(:,1) == 20170421, 1);
    histogram(allprops{i,1}(a:end,1), 20);%, 'normalization','probability');
    set(gca, 'YScale','log');
    xlabel('Normal force (N)')
    ylabel('Log bin count')
    lims(i,1,:) = axis;
    
    subplot(3,3,i+3)
    a = find(allsrcs{i,1}(:,1) == 20170421, 1);
    histogram(allprops{i,1}(a:end,2), 20);%, 'normalization','probability');
    set(gca, 'YScale','log')
    xlabel('Tangential speed (mm/s)')
    ylabel('Log bin count')
    lims(i,2,:) = axis;
    
    subplot(3,3,i+6)
    a = find(allsrcs{i,2}(:,1) == 20170421, 1);
    histogram(allprops{i,2}(a:end,2), 'BinEdges',linspace(0,80,20));
    xlabel('Impact force (N)')
    ylabel('Bin count')
    lims(i,3,:) = axis;
end

for i=1:3
    lim = [min(lims(:,i,1)) max(lims(:,i,2)) min(lims(:,i,3)) max(lims(:,i,4))];
    axis(subplot(3,3,3*(i-1)+1), lim);
    axis(subplot(3,3,3*(i-1)+2), lim);
    axis(subplot(3,3,3*(i-1)+3), lim);
end

print_subplots('fvhist_%d.pdf', 24);
