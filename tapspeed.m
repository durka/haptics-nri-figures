function props = tapspeed(d, do_fit, display)

    if nargin < 3
        display = true;
        if nargin < 2
            do_fit = true;
        end
    end

    props = zeros(0, 3);
    
    % separate tapping regions
    % TODO segment each tap from regions w/ multiple taps
    comp = bwconncomp(d.tmask);

    if display
        clf
    end
    for i=1:comp.NumObjects
        px = comp.PixelIdxList{i};

        % plot 1: end-effector Z position
        % find descent-to-tap and calc avg speed
        
        if display
            subplot(3,comp.NumObjects,i)
            plot(d.bvei(px,4))
            axis tight
            maxis([3 4], [-20 60]);
        end

        [~,tc] = min(d.bvei(px,4)); % index of surface contact (minimum Z position)
        [~,p] = findpeaks(d.bvei(px,4));
        tp = max([1; p(p<tc)]); % index of last peak before surface contact
        if display
            hold on
            plot(tc, d.bvei(px(1)+tc,4), 'r.', 'markersize',20)
            plot(tp, d.bvei(px(1)+tp,4), 'g.', 'markersize',20)
            hold off
        end

        % speed = rise/run
        down_vel = (d.bvei(px(1)+tc,4) - d.bvei(px(1)+tp,4))/(tc-tp)*3000;
        if display
            title(sprintf('%g', down_vel))
        end

        % plot 2: forces
        % find max impact force
        % also locate ringing after tap for next step
        
        if display
            subplot(3,comp.NumObjects,i+comp.NumObjects)
            plot(d.biws(px,2:4))
            axis tight
            maxis([3 4], [-10 60]);
        end

        [fc, tc] = max(d.biws(px,4)); % index of max force
        tw = find(diff(d.biws((px(1)+tc+50):end,4)) > 0, 1)+50; % start of ringing (first increase in force after peak, w/ 1/60 s grace period)
        ring_time = 300; % arbitrary decision that ringing lasts 0.1 s
        if display
            title(sprintf('%g', fc));
            rectangle('Position',  [tc+tw -10 ring_time 70], ...
                      'FaceColor', [0 0 0 0.5], ...
                      'EdgeColor', 'none'); % highlight ringing
        end

        % plot 3: ringing after tap in Z force
        % fit to decaying exponential and find envelope time constant
              
        if display
            subplot(3,comp.NumObjects,i+2*comp.NumObjects)
        end
        f = d.biws((px(1)+tc+tw):(px(1)+tc+tw+ring_time),4); % f = ringing part
        x = (1:length(f))';
        if do_fit
            % fit to decaying exponential
            [ff, gof] = fit(x, f, fittype('a*exp(-b*x)*sin(c*x+d)+e'), ...
                     fitoptions('Method',      'NonlinearLeastSquares', ...
                                'MaxFunEvals', 10000, ...
                                'MaxIter',     10000, ...
                                'StartPoint',  [1 0 1 0 0], ... % start with f = sin(x)
                                'Lower',       [-inf    0 -inf -inf -inf], ... % don't let time constant go negative
                                'Upper',       [ inf  inf  inf  inf  inf]));
            
            if display
                plot(x, f, x, ff.a*exp(-ff.b*x).*sin(ff.c*x+ff.d)+ff.e);
                title(sprintf('%.3f (%.2f)', ff.b*1000, gof.rsquare));
            end
        else
            if display
                plot(x, f)
            end
            
            % if we're not doing the fit just fake this so the conditional below succeeds
            ff = struct('b', 0);
            gof = struct('rsquare', 1);
        end
        if display
            axis tight
            maxis([3 4], [-4 4]);
        end
        
        % throw out this tap if it didn't fit or the peak isn't sharp
        % enough (because it's probably a false positive in that case)
        if gof.rsquare > 0.8 && ~isempty(findpeaks(d.biws(px,4), 'MinPeakProminence',20))
            if display
                xlabel GOOD
            end
            props = [props; down_vel fc ff.b*1000];
        else
            if display
                xlabel BAD
            end
        end
    end
    
end
