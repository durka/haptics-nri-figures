function varargout = april_render(episode, mode, varargin)
    switch mode
        case {'tag', 'path'}
            varargout{:} = feval(mode, episode, varargin{:});
        otherwise
            error('april_render:bad_mode', 'Unknown render mode');
    end
end

function img = tag(d, idx)
    img = imread(fullfile(d.datadir, d.date, d.flow, num2str(d.i), sprintf('bluefox/bluefox%d.png', idx)));
    imagesc(img);
    hold on;

    for i=1:length(d.april(idx).ids)
        id = d.april(idx).ids(i);
        cx = d.april(idx).centers(1,i);
        cy = d.april(idx).centers(2,i);
        x = [d.april(idx).p1s(1,i)
             d.april(idx).p2s(1,i)
             d.april(idx).p3s(i,1)
             d.april(idx).p4s(1,i)];
        y = [d.april(idx).p1s(2,i)
             d.april(idx).p2s(2,i)
             d.april(idx).p3s(i,2)
             d.april(idx).p4s(2,i)];
        x = x([1 2 3 4 1 3 2 4]);
        y = y([1 2 3 4 1 3 2 4]);
        plot(x, y, 'r');
        h = text(cx, cy, sprintf('%d', id));
        plot(cx, cy, 'r.', 'MarkerSize',9);
        h.FontSize = 8;
        h.Margin = 1;
        h.Color = [0 .75 0];
        h.BackgroundColor = [.8 .8 .8];
    end

    hold off;
end

function bbox = path(d, fa)
    bt = readtable(fullfile(d.datadir, d.date, d.flow, num2str(d.i), 'bluefox/bluefox_times.csv'));

    % find flat part
    half = round(size(d.bvei,1)/2);
    onethird = round(size(d.bvei,1)/3);
    twothirds = round(2*size(d.bvei,1)/3);
    start = onethird;
    stop = twothirds;

    if nargin == 1
        % get frame num with lowest endeff y-coord
        [~,tas] = sort(d.bvei(start:stop,3));
        for i=1:length(tas)
            ta = tas(i) + start;
            [~,fa] = min(abs(d.bvei(ta,1) - bt.UnixTimestamp));
            fa = bt.FrameNumber(fa);
            if length(d.april(fa).ids) > 20
                break;
            end
        end
    end

    img = imread(fullfile(d.datadir, d.date, d.flow, num2str(d.i), sprintf('bluefox/bluefox%d.png', fa)));
    imagesc(img);
    hold on;

    pt = [778 760];
    pts = [];
    apr = d.april;
    apra = apr(fa);
    for fi=sort(cell2mat(apr.keys))
        [~,ti] = min(abs(d.bvei(:,1) - bt.UnixTimestamp(bt.FrameNumber == fi)));
        if ti < start || ti > stop
            continue;
        end
        aprb = apr(fi);
        if length(aprb.ids) > 20
            [~,ia,ib] = intersect(apra.ids, aprb.ids);
            if length(ia) >= 4
                ctra = apra.centers(:,ia)';
                ctrb = aprb.centers(:,ib)';
                H = fitgeotrans(ctrb, ctra, 'similarity');

                ptb = [pt 1] * H.T;
                if ptb > 0
                    pts(end+1,:) = ptb(1:2);
                end
            end
        end
    end
    bbox = round([min(pts(:,1))-25 max(pts(:,2))+25 max(pts(:,1))+25 min(pts(:,2))-25]);
    bbox(1) = max(bbox(1), 1);           % left
    bbox(2) = min(bbox(2), size(img,1)); % bottom
    bbox(3) = min(bbox(3), size(img,2)); % right
    bbox(4) = max(bbox(4), 1);           % top
    x = [bbox(1) bbox(3)];
    y = [bbox(4) bbox(2)];
    x = x([1 2 2 1 1]);
    y = y([1 1 2 2 1]);

    plot(pts(:,1), pts(:,2), 'r');
    plot(x, y, 'r');

    hold off;
end

