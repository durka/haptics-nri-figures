function quiet(code, warnings)

    if ~iscell(warnings)
        warnings = {warnings};
    end
    
    states = cell(1,length(warnings));
    for w=1:length(warnings)
        ws = warning('off', warnings{w});
        states{w} = ws.state;
    end
    
    evalin('caller', code);
    
    for w=1:length(warnings)
        warning(states{w}, warnings{w});
    end

end
