%% setup

addpath(genpath('RANSAC-Toolbox'));
addpath(genpath('matGeom'));
addpath('../steinbach/htk-mfcc');
DATADIRS = {
    '/mnt/usbstick/proton_data'
    '/mnt/vertical/proton_data'
    '/mnt/matti/data'
    '/mnt/white/proton_data'
    '~/ssd/data'
    '/mnt/mybook/proton_data'
};
OUTDIR = '/mnt/mybook/proton_data';
cal_results;
calibs = containers.Map;
calibs('stickcam') = {'20170523', '1'};
calibs('optocam')  = {'20170523', '2'};
calibs('biocam')   = {'20170215', '3'};

%% list datasets

csv = readtable('surfaces_checked.csv', 'Format','%q%q%*q%q%q%q%*q%q%q%q%*q%q%q%*q%q%q%*[^\n]');
categories = unique(csv.MINCLabel);
specs = zeros(0,4);
for i=1:height(csv)
    if ~isempty(csv.SR{i}) && str2num(csv.SR{i}) > 1
        specs = [specs; str2num(csv.StickDate{i}) 1 str2num(csv.StickNum{i}) find(strcmp(categories, csv.MINCLabel{i}),1)];
    end
    if ~isempty(csv.OR{i}) && str2num(csv.OR{i}) > 1
        specs = [specs; str2num(csv.OptoDate{i}) 2 str2num(csv.OptoNum{i}) find(strcmp(categories, csv.MINCLabel{i}),1)];
    end
    if ~isempty(csv.BR{i}) && str2num(csv.BR{i}) > 1
        specs = [specs; str2num(csv.BioDate{i}) 3 str2num(csv.BioNum{i}) find(strcmp(categories, csv.MINCLabel{i}),1)];
    end
end

checked_props = containers.Map;

%% load and process

effs = {'stickcam' 'optocam' 'biocam'};
dates = unique(specs(:,1));
for dat=1:length(dates)
    date = num2str(dates(dat));

    for eff=1:length(effs)
        flow = effs{eff};
        all_nums = sort(specs(specs(:,1)==str2num(date) & specs(:,2)==eff, 3));
        batches = 1:5:length(all_nums);
        for b=1:length(batches)
            nums = all_nums(batches(b):(min(batches(b)+4,end)));
            if length(unique(nums)) < length(nums)
                error('duplicate episode');
            end
            if ~isempty(nums)
                tnow = clock;
                fprintf('\n[%02d:%02d] PROCESSING %d/%d %s %s %s\n\n', tnow(4), tnow(5), dat, length(dates), date, flow, mat2str(nums'));

                if exist(fullfile(OUTDIR, sprintf('%s_%s_%d.png', date, flow, nums(1))), 'file')
                    continue;
                end

                DATADIR = 'nope';
                for di=1:length(DATADIRS)
                    if exist(fullfile(DATADIRS{di}, date, flow), 'file')
                        DATADIR = DATADIRS{di};
                        break;
                    end
                end
                if strcmp(DATADIR, 'nope')
                    error('missing data');
                end

                clear data materials calib;
                clf;
                [data, materials, calib] = icra17_load(DATADIR, date, flow, @(x) x~=nums, calibs(flow));
                data = icra17_process('bluefox', data, calib, H_vic2bod, H_m402bod, H_bal2imu);

                if eff == 1
                    N = 4;
                else
                    N = 5;
                end
                for m=1:length(materials)
                    d = data(materials{m});
                    props = containers.Map;

                    flowrec = parse_flow(fullfile(DATADIR, date, flow, d.i, [flow '.flow']));
                    props('ratings') = flowrec.answers;

                    [~, taps] = icra17_process_taps(d);
                    tmask = false(size(d.tmask));
                    hmask = false(size(d.tmask));
                    for i=1:length(taps)
                        hmask(taps(i)-750:taps(i)-50) = 1;
                        tmask(taps(i)-50:taps(i)+750) = 1;
                    end

                    feats = {'MF' 'H' 'SC' 'TR' 'WV' 'SP' 'F' 'RG' 'Fr' 'FM'};
                    rom = [];
                    stein = [];
                    comp = bwconncomp(d.smask);
                    for c=1:length(comp.PixelIdxList)
                        if nnz(comp.PixelIdxList{c}) >= 1500
                            stein = [stein; steinbach_features(feats, hmask, tmask, comp.PixelIdxList{c}([1 end]), struct('data', dft321(d.bai(:,2:4)), 'Fs', 3000), struct('data', sqrt(sum(d.biws(:,2:3).^2,2)), 'Fs', 3000), [])];
                        end

                        pre = romano_features('pre', d.biws, d.bvei, d.bai, calib.mass, 0.25*3000, 0, comp.PixelIdxList{c}([1 end]));
                        rom = [rom; romano_features('post', pre, 10, 'naive', 0.1, 0)];
                    end
                    props('romano') = mean(rom);
                    props('steinbach') = mean(stein);

                    checked_props(sprintf('%s_%s_%s', date, flow, d.i)) = props;

                    clf;
                    subplot(N,1,1);
                    plot(d.bvei(:,1)-d.bvei(1,1), d.bvei(:,2:4));
                    ylabel('Position');
                    subplot(N,1,2);
                    plot(d.biws(:,1)-d.biws(1,1), d.biws(:,2:4));
                    ylabel('Force');
                    subplot(N,1,3);
                    plot(d.bai(:,1)-d.bai(1,1), d.bai(:,2:4));
                    ylabel('Vibration');
                    subplot(N,1,4);
                    plot(d.imu.mic(:,1)-d.imu.mic(1,1), d.imu.mic(:,2));
                    ylabel('Sound');
                    if eff == 2
                        subplot(N,1,5);
                        plot(d.sensor.opto(:,1)-d.sensor.opto(1,1), d.sensor.opto(:,2:4));
                        ylabel('Opto');
                    elseif eff == 3
                        subplot(N,1,5);
                        plot(d.sensor.bio(:,1)-d.sensor.bio(1,1), d.sensor.bio(:,2:end));
                        ylabel('Bio');
                    end
                    suplabel(sprintf('%s (%s)', materials{m}, flowrec.answers('surface name').text), 't');
                    print('-dpng', '-r200', fullfile(OUTDIR, sprintf('%s_%s_%s.png', date, flow, d.i)));
                end
            end
        end
    end
end

%% make tables

surfs = {};
ratings = zeros(0,6);
for i=1:height(csv)
    row = nan(1,6);
    if ~isempty(csv.SR{i}) && str2num(csv.SR{i}) > 1
        fprintf('stick %d\n', i);
        row(1) = str2num(subsref(checked_props, ...
                         substruct('()', {sprintf('%s_stickcam_%s', csv.StickDate{i}, csv.StickNum{i})}, ...
                                   '()', {'ratings'}, ...
                                   '()', {'slippery/sticky'}, ...
                                   '.',  'text')));
    end
    if ~isempty(csv.OR{i}) && str2num(csv.OR{i}) > 1
        fprintf('opto %d\n', i);
        row(2) = str2num(subsref(checked_props, ...
                         substruct('()', {sprintf('%s_optocam_%s', csv.OptoDate{i}, csv.OptoNum{i})}, ...
                                   '()', {'ratings'}, ...
                                   '()', {'slippery/sticky'}, ...
                                   '.',  'text')));
    end
    if ~isempty(csv.BR{i}) && str2num(csv.BR{i}) > 1
        props = {'slippery/sticky' 'soft/hard' 'smooth/rough' 'cool/warm'};
        for p=1:length(props)
            fprintf('bio %d %d\n', i, p);
            row(2+p) = str2num(subsref(checked_props, ...
                               substruct('()', {sprintf('%s_biocam_%s', csv.BioDate{i}, csv.BioNum{i})}, ...
                                         '()', {'ratings'}, ...
                                         '()', props(p), ...
                                         '.',  'text')));
        end
    end
    if ~all(isnan(row))
        surfs = [surfs; {csv.Surface{i} csv.SurfaceLocation{i}}];
        ratings = [ratings; row];
    end
end

writetable(table(surfs(:,1), surfs(:,2), ratings(:,1), ratings(:,2), ratings(:,3), ratings(:,4), ratings(:,5), ratings(:,6), 'VariableNames',{'Surface' 'Location' 'SS' 'OS' 'BS' 'H' 'R' 'W'}), 'ratings_checked.csv')

