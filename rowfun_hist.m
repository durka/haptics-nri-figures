function rowfun_hist(fun, table, group, input, nbins, ti, xl, yl)

    x = rowfun(fun, table, 'GroupingVariable',group, 'InputVariables',input, 'OutputVariableName','OUTPUT');
    histogram(x.OUTPUT, nbins);
    
    xlabel(xl);
    ylabel(yl);
    title(ti);

end
