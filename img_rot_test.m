%% setup
addpath(genpath('RANSAC-Toolbox'));
addpath(genpath('matGeom'));
DATADIRS = {
    '/mnt/usbstick/proton_data'
    '/mnt/vertical/proton_data'
    '~/ssd/data'
};
load icra17_final H_vic2bod H_m402bod H_bal2imu

%% list datasets

calibs = containers.Map;
%calibs('stickcam') = {'20170220', '2'};
%calibs('optocam')  = {'20170215', '2'};
calibs('stickcam') = {'20170523', '1'};
calibs('optocam')  = {'20170523', '2'};
calibs('biocam')   = {'20170215', '3'};

%dates = [221 222 223 225 227 301 303 304 306 307 308 309 312 321 323 403 404 405 406 407 413 418 419 420 421 426 427 429 501 502 503 504 508 509 516 518 519 523 613 614 615 620 621 626 627 630 705 724 727 811 828 829 831];
dates = [613 614 615 621 626 627 630 705 724 727 811 906 907 915 917 921 922 1004 1005 1008];
specs = {};
for i = 1:length(dates)
    specs = [ specs;
              {
                  { sprintf('2017%04d', dates(i)), 'stickcam' };
                  { sprintf('2017%04d', dates(i)), 'optocam'  };
                  { sprintf('2017%04d', dates(i)), 'biocam'   };
              }
            ];
end

%% load and process

alldata = containers.Map;
allmat = containers.Map;
errors = {containers.Map containers.Map};

for q = 1:length(specs)
    date = specs{q}{1};
    flow = specs{q}{2};
    fprintf('PROCESSING %s/%s\n', date, flow);

    for di=1:length(DATADIRS)
        if exist(fullfile(DATADIRS{di}, date), 'file')
            DATADIR = DATADIRS{di};
            break;
        end
    end

    checkfile = @(x,s) exist(fullfile(DATADIR, date, flow, num2str(x), 'crops'), 'file') > 0;

    try
       %if exist(fullfile(DATADIR, date, flow, 'props.mat'), 'file') > 0
       %    continue;
       %end

        if any(strcmp(alldata.keys, sprintf('%s%s', date(end-3:end), flow(1))))
            fprintf('\t(already loaded)\n');
            data = alldata(sprintf('%s%s', date(end-3:end), flow(1)));
            materials = allmat(sprintf('%s%s', date(end-3:end), flow(1)));
        else
            [data, materials, calib] = icra17_load(DATADIR, date, flow, @(x) false, calibs(flow));
           %for m=1:length(materials)
           %    d = data(materials{m});
           %    [d, taps] = segment_episode(d);
           %    d.taps = taps;
           %    data(materials{m}) = d;
           %end
            data = icra17_process('bluefox', data, calib, H_vic2bod, H_m402bod, H_bal2imu);
        end

        alldata(sprintf('%s%s', date(end-3:end), flow(1))) = data;
        allmat(sprintf('%s%s', date(end-3:end), flow(1))) = materials;
       %save(fullfile(DATADIR, date, flow, 'props.mat'), 'data');
        clear data materials;
        save -v7.3 all.mat alldata allmat
    catch err
        fprintf('error during outer loop: %s\n', err.message);
        disp(getReport(err));
        errors{1}(sprintf('%s_%s', date, flow)) = err;
        %keyboard
        % continue loop
    end
end

